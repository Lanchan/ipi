#include "binary.h"

Binary::Binary(Expression *left, Expression *right)
    : left(left), right(right)
{}

Binary::~Binary()
{
    delete left;
    delete right;
}
