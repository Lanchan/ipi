#pragma once

#include "binary.h"

// for an explanation take a look at sum.h
// the idea is the same but the implementation is slightly different
class Power : public Binary
{
public:
    Power(Expression *left, Expression *right);

    std::string str() const override;
    double value() const override;
};
