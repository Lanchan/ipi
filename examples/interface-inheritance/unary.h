#pragma once

#include "expression.h"

class Unary : public Expression
{
protected:
    // a unary expression consists of a single argument or operand
    Expression* argument;

public:
    // we can initialize the operand when constructing
    explicit Unary(Expression* argument);

    // the destructor will just delete the argument
    ~Unary() override;
};
