#pragma once

#include "binary.h"

class Sum : public Binary
{
public:
    // this is a limitation in C++: we have to implement the same constructor as in the Binary class
    // and simply pass the arguments
    Sum(Expression* left, Expression* right);

    // we want to override the pure virtual methods here
    // as a result the class Sum won't be abstract anymore
    std::string str() const override;
    double value() const override;
};
