#pragma once

#include "unary.h"

class Sqrt : public Unary
{
public:
    // this is a limitation in C++: we have to implement the same constructor as in the Unary class
    // and simply pass the argument
    explicit Sqrt(Expression *argument);

    // we want to override the pure virtual methods here
    // as a result the class Constant won't be abstract anymore
    std::string str() const override;
    double value() const override;
};
