#include <iostream>

#include "expression.h"
#include "power.h"
#include "constant.h"
#include "sum.h"
#include "sqrt.h"


int main()
{
    Expression *root = new Sqrt(
        new Sum(
            new Power(new Constant(6), new Constant(2)),
            new Sum(
                new Power(new Constant(10), new Constant(2)),
                new Power(new Constant(15), new Constant(2))
            )
        )
    );

    std::cout << root->str() << " = " << root->value() << std::endl;
    delete root;
}