#pragma once

#include <string>

class Expression
{
public:
    // we want to override the str() method in the child classes so we make it virtual
    // '= 0' is a special C++ syntax and means: I don't want to implement the method in this class.
    // I just want to implement it in a child class. We define the "interface" here.
    // As a result we cannot create an object of type Expression, because str() is not implemented.
    // such a method is called "pure virtual". A class with at least one "pure virtual" method is called "abstract"
    virtual std::string str() const = 0;

    // same goes for value()
    virtual double value() const = 0;

    // in a class hierarchy the destructor should ALWAYS be virtual. It cannot be "pure virtual" though
    virtual ~Expression() = default;
};