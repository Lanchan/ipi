#pragma once

#include "expression.h"

class Constant : public Expression
{
private:
    // this class simply encapsulates a double value
    double constant_value = 0;

public:
    // we can initialize it with a double - obviously
    explicit Constant(double constant_value);

    // we want to override the pure virtual methods here
    // as a result the class Constant won't be abstract anymore
    std::string str() const override;
    double value() const override;
};
