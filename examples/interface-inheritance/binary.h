#pragma once

#include "expression.h"

class Binary : public Expression
{
protected:
    // a binary expression consists of the left and right operands/arguments
    Expression* left;
    Expression* right;
public:
    // with the constructor we initialize the operands/arguments
    Binary(Expression* left, Expression* right);
    // the destructor will delete the children
    ~Binary() override;
};
