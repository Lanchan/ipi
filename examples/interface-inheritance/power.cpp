#include "power.h"

#include <cmath>

Power::Power(Expression *left, Expression *right)
    : Binary(left, right)
{}

std::string Power::str() const
{
    return '(' + left->str() + " ^ " + right->str() + ')';
}

double Power::value() const
{
    return std::pow(left->value(), right->value());
}


