#include "sum.h"


Sum::Sum(Expression *left, Expression *right)
        : Binary(left, right)
{}

double Sum::value() const
{
    return left->value() + right->value();
}

std::string Sum::str() const
{
    return '(' + left->str() + " + " + right->str() + ')';
}
