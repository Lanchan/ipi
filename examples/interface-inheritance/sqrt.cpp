#include "sqrt.h"

#include <cmath>

Sqrt::Sqrt(Expression *argument)
    : Unary(argument)
{}

std::string Sqrt::str() const
{
    return "sqrt " + argument->str();
}

double Sqrt::value() const
{
    return std::sqrt(argument->value());
}
