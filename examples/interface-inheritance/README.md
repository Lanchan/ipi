Interface Inheritance
=====================

In this example a class hierarchy for modeling arithmetic expressions is implemented. The top most base class
is `Expression` which has two methods: `value()` which will calculate the value of the expression and `str()` to
build a string representation of the expression.

***Note***: you will see the keywords `const` and `override` very often in the classes. Both can be omitted without changing the meaning of the program. They are just considered "good style".

#### Here is an overview of the class hierarchy:


- `Expression` (header only) has the two methods plus a virtual destructor (**important**)
  - `Binary`: this provides a base class for binary expressions like addition. The member variables are the left and right operands. The destructor simply delegates the deletion to the operands.
    - `Sum`: this is a concrete class. It implements the addition of two sub expressions. For that it implements `value()` by adding the values of the operands and `str()` by concatenating the string representation of the left operand, a plus sign and the right operand
    - `Power`: this is a concrete class. It implements the exponentiation of two sub expressions. `value()` is implemented to calculate `std::pow` of the left and right operands. `str()` is similar to `Addition::str()`
  - `Unary`: this is a base class for unary expressions like unary functions. It stores the single argument and delegates the deletion to it when the destructor is called.
    - `Sqrt`: the concrete class for calculating the square root. `value()` simply calls `std::sqrt` and `str()` formats the argument like this: `"sqrt (argument)"`
  - `Constant`: the "leafs" of the operator tree. This class stores a double constant and returns it value when `value()` is called. `str()` just converts the value to string.


#### Here are some example usages:

##### The least complex case

```cpp
Expression* root = Constant(42);

std::cout << root->str() << " = " << root->value() << std::endl;
// prints: 42 = 42

delete root;  // don't forget this!
```

##### Square root of two

```
Expression* root = new Sqrt(new Constant(2));

std::cout << root->str() << " = " << root->value() << std::endl;
// prints: sqrt 2 = 1.41421

delete root;
```

##### Or the same with power

```
Expression* root = new Power(new Constant(2), new Constant(0.5));

std::cout << root->str() << " = " << root->value() << std::endl;
// prints: 2 ^ 0.5 = 1.41421

delete root;
```

You can build arbitrary expressions by nesting the classes further. Take a look at [`main.cpp`](main.cpp). For more details take a look inside the files.

#### Exercise: Implement Multiplication

- Add a new class `Product`
- Decide which of the classes you would inherit from
- Implement `Product::value()` similarly to `Sum::value()`
- Implement `Product::str()` similarly to `Sum::str()`
- Calculate `2 * 3 * 4 * 5` and print the expression