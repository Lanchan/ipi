#include "constant.h"

Constant::Constant(double constant_value)
    : constant_value(constant_value)
{}

double Constant::value() const
{
    return constant_value;
}

std::string Constant::str() const
{
    return std::to_string(constant_value);
}
