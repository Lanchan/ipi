// Just add this. It prevents some errors
// NEVER EVER use "using namespace" in a header file
#pragma once

#include <iostream>

class Fraction
{
private:
    // i added an underscore "_" so i can call the getters numerator() and denominator()
    long int _numerator;
    long int _denominator;
public:
    Fraction(long int numerator=0, long int denominator=1);
    // this defines the copy constructor for free
    Fraction(const Fraction&) = default;

    // this method CHANGES the fraction and thus cannot be marked with "const"
    void reduce();

    // these methods don't change the fraction -> mark the "const"
    long int numerator() const;
    long int denominator() const;
    double decimal() const;
    // the setters cannot be const
    void setNumerator(long int numerator);
    void setDenominator(long int denominator);

    // this is also const as it returns a modified copy
    Fraction reciprocal() const;

    // some operators
    // the binary operators are const (they don't change the object)
    //     and they take const reference arguments (they also don't change the arguments)
    Fraction operator*(const Fraction& other) const;
    Fraction operator/(const Fraction& other) const;
    Fraction operator+(const Fraction& other) const;
    Fraction operator-(const Fraction& other) const;

    // a static method example. It does not depend of an actual Fraction instance
    static Fraction fromDouble(double d);

    // due to popular demand: how to enable static casting from Fraction to double
    // wait what?
    // No return type? correct
    // double is an operator? not really
    // but it should be const
    operator double() const;

};

// this lets us "cout" a fraction
// note that "out" is a non-const reference while f should be a const reference
std::ostream& operator<<(std::ostream& out, const Fraction& f);