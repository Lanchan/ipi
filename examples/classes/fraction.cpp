#include <cmath>

#include "fraction.h"

// this function can only be used inside this file as we didn't declare it in the header
int gcd(long int a, long int b)
{
    return (b == 0)? a : gcd(b, a % b);
}

Fraction::Fraction(long int numerator, long int denominator)
    : _numerator(numerator), _denominator(denominator)  // this is the preferred way to initialize members
{
    reduce();
}

// we don't have to implement the copy constructor as we marked it with "default"

void Fraction::reduce()
{
    if(_numerator == 0)
    {
        _denominator = 1;
        return;
    }

    if(_denominator < 0)
    {
        _denominator = -_denominator;
        _numerator = -_numerator;
    }

    int factor = gcd(std::abs(_numerator), _denominator);
    _numerator /= factor;
    _denominator /= factor;
}

long int Fraction::numerator() const // notice the "const" here again
{
    return _numerator;
}

long int Fraction::denominator() const
{
    return _denominator;
}

double Fraction::decimal() const
{
    return double(_numerator) / _denominator;
}

void Fraction::setNumerator(long int numerator)
{
    _numerator = numerator;
}

void Fraction::setDenominator(long int denominator)
{
    _denominator = denominator;
}

Fraction Fraction::reciprocal() const
{
    return {
        _denominator,
        _numerator
    };
}

Fraction Fraction::operator*(const Fraction& other) const
{
    return {
        _numerator * other._numerator,  // we can access the private members of "other" because it's also a fraction
        _denominator * other._denominator
    };
}

Fraction Fraction::operator/(const Fraction &other) const
{
    return {
        _numerator * other._denominator,
        _denominator * other._numerator
    };
}

Fraction Fraction::operator+(const Fraction &other) const
{
    return {
        _numerator * other._denominator + other._numerator * _denominator,
        _denominator * other._denominator
    };
}

Fraction Fraction::operator-(const Fraction &other) const
{
    return {
            _numerator * other._denominator - other._numerator * _denominator,
            _denominator * other._denominator
    };
}

Fraction::operator double() const
{
    return decimal();
}

Fraction Fraction::fromDouble(double d)
{
    // how do you create a fraction from a decimal number?
    // you can use this bad "algorithm"
    return {
        static_cast<long int>(std::round(d * 10000)),
        10000
    };
}

std::ostream& operator<<(std::ostream& out, const Fraction& f)
{
    // note that i can only use f.denominator() on a const Reference because
    //   the "denominator()" method is marked with "const"
    // without "const" at the end this would not compile. try it!
    if(f.denominator() == 1)
    {
        return out << f.numerator();
    }
    return out << f.numerator() << '/' << f.denominator();
}