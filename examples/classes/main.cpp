#include <iostream>
#include <cassert>

// We only ever include Header files. Never include a CPP or CC file
#include "fraction.h"

// compile this project with
//   g++ -Wall -Wextra -pedantic fraction.cpp main.cpp -o fraction
// or install cmake :p

int main()
{
    // note that there are now a lot of ways to initialize a fraction
    // pick your favourite and don't mix them
    // most of these are equivalent and some only work under certain circumstances
    // you might not believe it but all variants except six_thirteenth only use the constructor Fraction(long, long)
    // welcome to C++
    Fraction third{1, 3};                       // curly braces
    Fraction two_fifth = {2, 5};                // equals and  curly braces
    Fraction three_seventh(3, 7);               // round parenthesis
    Fraction four_ninth = Fraction(4, 9);       // this with parenthesis
    Fraction one = 1;                           // wat (this works because Fraction one(1); works)
    Fraction five_eleventh = Fraction{5, 11};   // this with curly braces
    Fraction six_thirteenth;                    // this one is too much work
    six_thirteenth.setNumerator(6);
    six_thirteenth.setDenominator(13);
    Fraction seven_fifteenth{7, 15};
    Fraction eight_seventeenth{8, 17};

    // Wikipedia said that this is an approximation for pi/2
    Fraction pi_half =
            one +
            third +
            third*two_fifth +
            third*two_fifth*three_seventh +
            third*two_fifth*three_seventh*four_ninth +
            third*two_fifth*three_seventh*four_ninth*five_eleventh +
            third*two_fifth*three_seventh*four_ninth*five_eleventh*six_thirteenth +
            third*two_fifth*three_seventh*four_ninth*five_eleventh*six_thirteenth*seven_fifteenth;

    Fraction pi = pi_half; // this uses the default copy-constructor
    pi.setNumerator(pi.numerator() * 2);
    pi.reduce();

    std::cout << "pi:      " << pi << std::endl;
    std::cout << "decimal: " << pi.decimal() << std::endl;
    std::cout << "what a bad approximation, thanks Wikipedia" << std::endl;

    // you can see: creating a fraction from a double does not depend on a particular fraction instance
    // the rule of thumb: If you don't use member variables or methods inside the method in question.
    //                    that very method should be static
    std::cout << std::endl << "0.666 ... as a fraction is: " << Fraction::fromDouble(2.0 / 3.0) << std::endl;

    std::cout << std::endl << "using static cast: " << static_cast<double>(pi) << std::endl;
    // we can even do this now:
    double pi_approx = pi;
    std::cout << "using implicit cast: " << pi_approx << std::endl;
    std::cout << "or double(pi) " << double(pi) << std::endl;
    std::cout << "or (double)pi " << (double)pi << "    but this is C, don't use it" << std::endl;

    // some testing:
    // this works because "==" can cast a fraction into a double and compare the results
    assert(Fraction() == Fraction(0, 1));
    assert(Fraction() == Fraction(0, 2));

    assert(Fraction(1) == Fraction(2, 2));

    assert(Fraction(1, 2) == Fraction(2, 4));

    Fraction test = {1, 2};
    test.setNumerator(3);
    assert(test == Fraction(3, 2));
    test.setDenominator(5);
    assert(test == Fraction(3, 5));
    test.setDenominator(6);
    assert(test == Fraction(3, 6));
    assert(test.numerator() != 1);  // test is not reduced yet
    test.reduce();
    assert(test.numerator() == 1);

    // why only have one cast if we can have 5?
    assert(Fraction(1, 2).decimal() == 0.5);
    assert(static_cast<double>(Fraction(1, 2)) == 0.5);
    assert(double(Fraction(1, 2)) == 0.5);
    assert((double)Fraction(1, 2) == 0.5);
    assert(Fraction(1, 2) == 0.5);

    assert(Fraction(1, 2) + Fraction(1, 2) == 1);
    assert(Fraction(1, 2) + Fraction(1, 3) == Fraction(5, 6));

    assert(Fraction(3, 4) * Fraction(5, 7) == Fraction(15, 28));
    assert(Fraction(2, 3) * Fraction(3, 4) == Fraction(1, 2));

    assert(Fraction::fromDouble(0.5) == Fraction(1, 2));
}