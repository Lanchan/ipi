IPI
===

What's new?
-----------

- **31.01.2018:** [sheet 11](solutions/11) solutions
- **31.01.2018:** [sheet 10](solutions/10) solutions
- **31.01.2018:** [sheet 9](solutions/09) solutions
- **29.12.2017:** [sheet 7](solutions/07) solutions
- **13.12.2017:** [sheet 6](solutions/06) solutions
- **04.12.2017:** [sheet 5](solutions/05) solutions
  - [*exercise 1*](solutions/05/structure.cc) structs
  - [*exercise 2*](solutions/05/pointer.cc) pointers
  - [*exercise 3*](solutions/05/recursive_fcns.cc) recursive functions + [*some plots*](solutions/05/README.md)
- **27.11.2017:** [sheet 4](solutions/04) solutions
  - [*exercise 1*](solutions/04/array_sort.cc) array sort
  - [*exercise 2*](solutions/04/geometric_objects.cc) geometric objects
  - [*exercise 3*](solutions/04/quadratic_equations.cc) quadratic equations
  - [*exercise 4*](solutions/04/roots.cc) roots
  - *exercise 5 (bonus)* matrices
     - [*with 2D arrays*](solutions/04/matrices.cc)
     - [*with `std::vector`*](solutions/04/matrices_std.cc)
     - [*with dynamic allocation*](solutions/04/matrices_struct.cc)
- **27.11.2017:** [sheet 3](solutions/03) solutions
  - [*exercise 1*](solutions/03/state.cpp) state machine
  - [*exercise 2*](solutions/03/einmaleins.cc) Einmaleins
  - [*exercise 3*](solutions/03/reihenberechnung.cc) Reihenberechnung
  - [*exercise 4*](solutions/03/ggt.cc) GGT
- **15.11.2017:** [sheet 2](solutions/02) solutions
  - [*exercise 3*](solutions/02/turing.cpp): turing machine

Links for Windows
-----------------

- [Compiler MinGW](https://sourceforge.net/projects/mingw-w64/)
- [Notepad++](https://notepad-plus-plus.org/)
- (optional) [Visual Studio](https://webapps.urz.uni-heidelberg.de/ms_imagine/ms_imagine_login_mita.php)


Linux (Ubuntu/Debian/Mint)
--------------------------

- Install gcc with `sudo apt-get install build-essential`


IDEs/Other editors for Linux+Windows (optional)
-----------------------------------------------

- [IDE CLion](https://www.jetbrains.com/clion/download/#section=windows)
- [Sublime Text 3](https://www.sublimetext.com/3)
- [Atom](https://atom.io/)
