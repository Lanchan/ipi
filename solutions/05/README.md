Plots for exercise 5.3
======================


Number of steps for function `power(double base, int exponent)`
---------------------------------------------------------------

![Number of steps for function `power(double base, int exponent)`](power_steps.png)

Comparison of `power()` and `power_wrong()`
-------------------------------------------

![Comparison of `power()` and `power_wrong()`](power_vs_wrong_steps.png)

Comparison of `power()` and `power_wrong()` (log scale)
-------------------------------------------------------

![Comparison of `power()` and `power_wrong()` (log scale)](power_vs_wrong_steps_logscale.png)
