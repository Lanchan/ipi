#include <iostream>
#include <cassert>

int sum(int n)
{
    if(n < 1)
    {
        return 0;
    }

    return n + sum(n - 1);
    // one-liner: return (n < 1)? 0 : n + sum(n - 1)
}

void convert_decimal(int value, int base)
{
    if(value < 0 || base < 2)
    {
        std::cerr << "can't handle this" << std::endl;
    }
    else if(value == 0)
    {
        std::cout << 0;
    }
    else
    {
        convert_decimal(value / base, base);
        int digit = value % base;
        if(digit > 9)
        {
            std::cout << char('A' + digit - 10);
        }
        else
        {
            std::cout << digit;
        }
    }
}

double square(double x)
{
    return x * x;
}

double power(double base, int exponent, int* steps=nullptr)
{
    if(exponent == 1)
    {
        return base;
    }

    // this just writes the number of steps to the steps variable and was not required
    if(steps)
    {
        *steps = *steps + 1;
    }

    if(exponent % 2 == 0)
    {

        return square(power(base, exponent / 2, steps));
    }
    else
    {
        return power(base, exponent - 1, steps) * base;
    }
}

double power_wrong(double base, int exponent, int* steps=nullptr)
{
    if(exponent == 1)
    {
        return base;
    }

    if(steps)
    {
        *steps = *steps + 1;
    }

    if(exponent % 2 == 0)
    {

        return power_wrong(base, exponent / 2, steps) * power_wrong(base, exponent / 2, steps);
    }
    else
    {
        return power_wrong(base, exponent - 1, steps) * base;
    }
}

int main()
{
    // a)
    std::cout << "a)" << std::endl;
    std::cout << "sum(12) = " << sum(12) << std::endl;
    assert(sum(-10) == 0);
    assert(sum(-1) == 0);
    assert(sum(0) == 0);
    assert(sum(1) == 1);
    assert(sum(2) == 3);
    assert(sum(12) == (12*13) / 2);
    std::cout << "all tests passed" << std::endl;

    // b)
    std::cout << std::endl << "b)" << std::endl;
    std::cout << "1278 in base  2: ";
    convert_decimal(1278, 2);
    std::cout << std::endl;
    std::cout << "1278 in base  8: ";
    convert_decimal(1278, 8);
    std::cout << std::endl;
    std::cout << "1278 in base 16: ";
    convert_decimal(1278, 16);
    std::cout << std::endl;

    // c)
    std::cout << std::endl << "c)" << std::endl;
    std::cout << "12^3 = " << power(12, 3) << std::endl;
    assert(power(2, 1) == 2);
    assert(power(2, 2) == 4);
    assert(power(2, 3) == 8);
    assert(power(2, 4) == 16);
    assert(power(2, 5) == 32);
    assert(power(2, 10) == 1024);
    std::cout << "all tests passed" << std::endl;

    std::cout << std::endl;
    std::cout << "  n  | power | wrong" << std::endl;
    std::cout << "-----+-------+-------" << std::endl;

    for(int i = 1; i <= 15; ++i)
    {
        std::cout.width(4);
        std::cout << i << " |";

        int steps = 0;
        power(2, i, &steps);
        std::cout.width(5);
        std::cout << steps << "  |";

        steps = 0;
        power_wrong(2, i, &steps);
        std::cout.width(5);
        std::cout << steps << std::endl;
    }
}

