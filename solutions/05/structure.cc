#include <iostream>
#include <cmath>
#include <limits>

int gcd(int a, int b)
{
    return (b == 0)? a : gcd(b, a % b);
}

struct fraction
{
    int numerator = 0;
    int denominator = 1;
};

std::ostream& operator<<(std::ostream& out, const fraction& f)
{
    if(f.numerator == 0 || f.denominator == 1)
    {
        return out << f.numerator;
    }
    return out << f.numerator << "/" << f.denominator;
}

void reduce(fraction& f)
{
    if(f.denominator == 0)
    {
        std::cerr << "dividing by zero is not allowed" << std::endl;
        return;
    }

    if(f.numerator == 0)
    {
        f.denominator = 1;
        return;
    }

    if(f.denominator < 0)
    {
        f.denominator *= -1;
        f.numerator *= -1;
    }

    int factor = gcd(std::abs(f.numerator), f.denominator);

    f.numerator /= factor;
    f.denominator /= factor;
}

fraction mult(const fraction& lhs, const fraction& rhs)
{
    fraction f = {
        lhs.numerator * rhs.numerator,
        lhs.denominator * rhs.denominator
    };
    reduce(f);
    return f;
}

fraction sum(const fraction& lhs, const fraction& rhs)
{
    fraction f = {
            lhs.numerator * rhs.denominator + rhs.numerator * lhs.denominator,
            lhs.denominator * rhs.denominator
    };
    reduce(f);
    return f;
}

fraction reciprocal(const fraction& f)
{
    if(f.numerator == 0)
    {
        std::cerr << "1/0 nope" << std::endl;
    }
    return {
        f.denominator,
        f.numerator
    };
}

fraction div(const fraction& lhs, const fraction& rhs)
{
    return mult(lhs, reciprocal(rhs));
}

double decimal(const fraction& f)
{
    if(f.denominator == 0)
    {
        return std::numeric_limits<double>::infinity() * f.numerator;
    }
    return f.numerator / double(f.denominator);
}

// or with operators
fraction operator+(const fraction& lhs, const fraction& rhs)
{
    return sum(lhs, rhs);
}

fraction operator*(const fraction& lhs, const fraction& rhs)
{
    return mult(lhs, rhs);
}

fraction operator/(const fraction& lhs, const fraction& rhs)
{
    return div(lhs, rhs);
}

int main()
{
    fraction a = {12, 53};
    fraction b = {3, 4};
    fraction c = {-1, 2};
    fraction d = {1, 5};

    std::cout << "with functions:" << std::endl;
    fraction result = div(sum(a, mult(b, c)), d);
    std::cout << result << std::endl;
    std::cout << decimal(result) << std::endl << std::endl;

    std::cout << "with oeprators:" << std::endl;
    result = (a + b*c) / d;
    std::cout << result << std::endl;
    std::cout << decimal(result) << std::endl << std::endl;
}