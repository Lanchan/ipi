#include <cmath>
#include <iostream>
#include <cassert>

// this one does not work because the values are COPIED into the function
// the function then swaps the copies
// this does not affect the original variables
void swap1(double a, double b)
{
    std::cout << "in swap 1:" << std::endl;;
    std::cout << "    " << a << ", " << b << std::endl;
    std::cout << "swapping ..." << std::endl;

    double tmp = a;
    a = b;
    b = tmp;

    std::cout << "    " << a << ", " << b << std::endl;
}

// this one works because the parameters are passed by reference
// the swap inside the function will affect the original values
void swap2(double& a, double& b)
{
    std::cout << "in swap 2:" << std::endl;;
    std::cout << "    " << a << ", " << b << std::endl;
    std::cout << "swapping ..." << std::endl;

    double tmp = a;
    a = b;
    b = tmp;

    std::cout << "    " << a << ", " << b << std::endl;
}

// this one works because the addresses of the variables are passed
// the function swaps the value that the pointer point to
// this affects the original variables
void swap3(double* a, double* b)
{
    std::cout << "in swap 3:" << std::endl;;
    std::cout << "    " << *a << ", " << *b << std::endl;
    std::cout << "swapping ..." << std::endl;

    double tmp = *a;
    *a = *b;
    *b = tmp;

    std::cout << "    " << *a << ", " << *b << std::endl;
}

double* maximum_value(double* a, int size)
{
    if(size == 0)
    {
        return nullptr;
    }

    int max_index = 0;
    for(int i = 1; i < size; ++i)
    {
        if(a[i] >= a[max_index])
        {
            max_index = i;
        }
    }
    return &a[max_index];
    // or return a + max_index;
}

int length_string(const char* str)
{
    int length = 0;

    while(*str != 0)
    {
        ++str;  // ++pointer -> "go to the next element"
        ++length;
    }
    return length;
}

void reverse_string(char* str)
{
    int length = length_string(str);

    char* start = str;
    char* end = str + length - 1;

    while(start < end)
    {
        char tmp = *start;
        *start = *end;
        *end = tmp;

        --end;
        ++start;
    }
}

int main()
{
    // a)
    std::cout << "a)" << std::endl;
    double value = (1 + std::sqrt(5)) / 2;
    double* pointer;

    pointer = &value;

    std::cout.precision(16);
    std::cout << "value is: " << value << std::endl;
    *pointer = 1 / *pointer;
    std::cout.precision(15);
    std::cout << "and now:  " << value << std::endl;

    // b)
    std::cout << std::endl << "b)" << std::endl;
    int a1 = 15;
    int a2 = 20;
    int* p1 = &a1;
    int* p2 = &a2;

    std::cout << "a: " << a1 << ", " << a2 << std::endl;
    std::cout << "p: " << p1 << ", " << p2 << std::endl;
    std::cout << "*p1 = *p2 -> write to the variable p1 points to by using the value that p2 points to" << std::endl;
    std::cout << "             p1 still points to a1 and p2 still points to a2" << std::endl;
    *p1 = *p2;
    std::cout << "a: " << a1 << ", " << a2 << std::endl;
    std::cout << "p: " << p1 << ", " << p2 << std::endl;

    // reset
    a1 = 15;

    std::cout << std::endl << "a: " << a1 << ", " << a2 << std::endl;
    std::cout << "p: " << p1 << ", " << p2 << std::endl;
    std::cout << "p1 = p2 -> change the variable that p1 points to to the variable that p2 points to" << std::endl;
    std::cout << "           p1 now points to a2 and p2 still points to a2" << std::endl;
    std::cout << "           a1 and a2 are not changed" << std::endl;
    p1 = p2;
    std::cout << "a: " << a1 << ", " << a2 << std::endl;
    std::cout << "p: " << p1 << ", " << p2 << std::endl;

    // c)
    std::cout << std::endl << "c)" << std::endl;
    double a = 7;
    double b = 12;

    std::cout << "before swap 1" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;
    swap1(a, b);
    std::cout << "after swap 1" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;

    std::cout << std::endl << "before swap 2" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;
    swap2(a, b);
    std::cout << "after swap 2" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;

    // reset
    a = 7;
    b = 12;

    std::cout << std::endl << "before swap 3" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;
    swap3(&a, &b);
    std::cout << "after swap 3" << std::endl;
    std::cout << "    " << a << ", " << b << std::endl;

    // d)
    std::cout << std::endl << "d)" << std::endl;
    double* empty = nullptr;
    double one[] = {1};
    double ascending[] = {1, 2, 3, 4};
    double descending[] = {4, 3, 2, 1};
    double mixed[] = {1, 3, 4, 2};
    double duplicate[] = {1, 1, 4, 1, 4, 1, 1, 4, 1};
    double sheet_test[] = {1., 4., 12., 3., 5.};

    // some test cases
    assert(maximum_value(empty, 0) == nullptr);
    assert(maximum_value(one, 1) == &one[0]);
    assert(maximum_value(ascending, 4) == &ascending[3]);
    assert(maximum_value(descending, 4) == &descending[0]);
    assert(maximum_value(mixed, 4) == &mixed[2]);
    assert(maximum_value(duplicate, 9) == &duplicate[7]);
    assert(maximum_value(sheet_test, 5) == &sheet_test[2]);
    std::cout << "all tests passed" << std::endl;

    // e)
    std::cout << std::endl << "e)" << std::endl;
    assert(length_string("") == 0);
    assert(length_string("1") == 1);
    assert(length_string("12") == 2);
    assert(length_string(" ") == 1);
    assert(length_string("Ihren Namen!") == 12);
    std::cout << "all tests passed" << std::endl;

    // f)
    std::cout << std::endl << "f)" << std::endl;
    char empty_string[] = "";
    char one_char[] = "1";
    char two_chars[] = "12";
    char even[] = "0123456789";
    char odd[] = "abcdefgh";
    char space[] = "Kevin Spacey";
    char name[] = "Ihren Namen!";

    reverse_string(empty_string);
    reverse_string(one_char);
    reverse_string(two_chars);
    reverse_string(even);
    reverse_string(odd);
    reverse_string(space);
    reverse_string(name);

    assert(length_string(empty_string) == 0);
    assert(one_char == std::string("1"));
    assert(two_chars == std::string("21"));
    assert(even == std::string("9876543210"));
    assert(odd == std::string("hgfedcba"));
    assert(space == std::string("yecapS niveK"));
    assert(name == std::string("!nemaN nerhI"));

    std::cout << "all tests passed" << std::endl;

}