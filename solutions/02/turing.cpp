// Note: you don't have to understand all of this
// The turing programs can be found in the main function

#include <iostream>     // cout and stuff
#include <iomanip>      // setw
#include <vector>       // vector, obviously


// These are the three movements a turing machine is capable of
enum class Move
{
    Left = -1,
    Stay = 0,
    Right = 1,
};

// The tape is just a string of characters
using Tape = std::string;
// The state is just a name
using State = std::string;
// The symbols are characters
using Symbol = char;

// The program of the turing machine consists of many actions
// Each action consists of
//      the current state and the symbol to read
//      the symbol to write, the movement and the new state
struct Action
{
    State initial_state;
    Symbol read;

    Symbol write;
    Move move;
    State new_state;
};

// A turing program is a "list" of actions
using Program = std::vector<Action>;


// Print the tape to the console and highlights the cursor
void print_state(Tape tape, int position, State state)
{
    // std::setw(k) is the same as cout.width(k) but can be used inline
    std::cout << ">" << std::setw(10) << std::left << state << "<  ";
    for(int i = 0; i < tape.size(); ++i)
    {
        if(i == position)   // if the cursor is on this position we add [ ] around the symbol
        {
            std::cout << '[' << tape[i] << ']';
        }
        else
        {
            std::cout << ' ' << tape[i] << ' ';
        }

    }
    std::cout << std::endl;
}

// Determine the numbers encoded by '1' on the tape
// 11101111 -> 3, 4
void print_unary(Tape tape, int cursor)
{
    if(tape[cursor] == '0')
    {
        std::cout << 0;
        return;
    }

    int counter = 0;
    for(int i = cursor; i < tape.size(); ++i)
    {
        if(tape[i] == '1')
        {
            ++counter;
        }
        else if(counter != 0)
        {
            std::cout << counter << ", ";
            counter = 0;
        }
    }
    if(counter != 0)
    {
        std::cout << counter;
    }
}

// Create a tape with 'start_position' leading '0's
// and for each element in 'inputs' adds n '1's separated by '0'
// make_tape(2, {2, 3}) -> 0 0 1 1 0 1 1 1 0
Tape make_tape(int start_position, std::vector<int> inputs)
{
    Tape band;

    for(int i = 0; i < start_position; ++i)
    {
        band += '0';
    }

    for(int input : inputs)
    {
        for(int i = 0; i < input; ++i)
        {
            band += '1';
        }
        band += '0';
    }
    return band;
}

// Run the program by checking the conditions in the program table and transitioning
// to the next state
void run_program(Program program, int start_position, std::vector<int> input)
{
    bool running = true;            // this is set to false if we hit an end state
    int steps = 0;                  // keep track of the number of total steps
    int position = start_position;  // the initial cursor position

    State state = program.at(0).initial_state;      // the initial state is the first in the table
    Tape band = make_tape(start_position, input);

    std::cout << "input: ";
    print_unary(band, position);
    std::cout << std::endl;

    while(running)
    {
        print_state(band, position, state);
        Symbol symbol = band[position];

        running = false;
        for(auto action : program)  // search an entry in the table that matches the current state
        {
            if(action.initial_state == state && action.read == symbol)
            {
                state = action.new_state;
                band[position] = action.write;
                position += static_cast<int>(action.move);
                running = true;
                ++steps;
                break;
            }
        }
    }
    std::cout << "output: ";
    print_unary(band, position);
    std::cout << std::endl;
    std::cout << "steps: " << steps << std::endl;
}

int main()
{
    // this turing program adds the two number on the tape
    // the number of steps are equal to 2*A + 1 with A being the first number
    Program add_program = {
        // state,       read,   write,  move,           next state
        {"initial",     '0',    '0',    Move::Stay,     "end"},
        {"initial",     '1',    '0',    Move::Right,    "replace 0"},

        {"replace 0",   '0',    '1',    Move::Left,     "go back"},
        {"replace 0",   '1',    '1',    Move::Right,    "replace 0"},

        {"go back",     '1',    '1',    Move::Left,     "go back"},
        {"go back",     '0',    '0',    Move::Right,    "end"}
    };

    // run the program with numbers 4 and 2 having 0 extra space on the left
    run_program(add_program, 0, {4, 2});

    std::cout << std::endl;

    // this is the turing program that doubles the number on the tape
    // number of steps: 2*N² + N
    Program double_program = {
        // state,       read,   write,  move,           next state
        {"init",        '0',    '0',    Move::Stay,     "end"},
        {"init",        '1',    '1',    Move::Right,    "special"},

        {"special",     '0',    '1',    Move::Left,     "end"},
        {"special",     '1',    '1',    Move::Left,     "start"},

        {"start",       '1',    '0',    Move::Left,     "add"},

        {"add",         '0',    '1',    Move::Right,    "find right"},

        {"find right",  '0',    '0',    Move::Right,    "find right"},
        {"find right",  '1',    '1',    Move::Right,    "not last"},

        {"not last",    '0',    '1',    Move::Left,     "skip"},
        {"not last",    '1',    '1',    Move::Left,     "found"},

        {"found",       '1',    '0',    Move::Left,     "find left"},

        {"find left",   '0',    '0',    Move::Left,     "find left"},
        {"find left",   '1',    '0',    Move::Left,     "add"},

        {"skip",        '1',    '1',    Move::Left,     "fill"},

        {"fill",        '0',    '1',    Move::Left,     "fill"},
        {"fill",        '1',    '1',    Move::Stay,     "end"}
    };

    // run the program with number 4 having 5 extra '0's on the left
    run_program(double_program, 5, {4});

    // You can insert your own program and see how it performs
}