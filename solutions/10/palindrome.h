#ifndef PALINDROME_H
#define P

#include <string>


bool is_palindrome(const std::string& word);

#endif
