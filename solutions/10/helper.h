#ifndef HELPER_H
#define HELPER_H

#include <vector>
#include <string>


void remove_lineending(std::string& word);

std::vector<std::string> read_file(const std::string& filename);

#endif
