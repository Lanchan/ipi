#include "palindrome.h"


bool is_palindrome(const std::string& word)
{
    if(word.size() < 3)
    {
        return false;
    }
    
    auto start = word.begin();
    auto end = word.end();
    while(start < end)
    {
        --end;
        if(*start != *end)
        {
            return false;
        }
        ++start;
    }
    return true;
}
