#include <iostream>
#include <iomanip>

#include "simpletests.h"

#include "helper.h"
#include "palindrome.h"

int main()
{
    test(is_palindrome("lol"));
    test(is_palindrome("LoL"));
    test(is_palindrome("otto"));
    test(is_palindrome("reliefpfeiler"));
    test(is_palindrome("reittier"));
    test(is_palindrome("radar"));
    test(is_palindrome("rentner"));

    test(!is_palindrome(""));
    test(!is_palindrome("a"));
    test(!is_palindrome("aa"));
    test(!is_palindrome("aa"));
    test(!is_palindrome("ab"));
    test(!is_palindrome("abc"));
    test(!is_palindrome("otti"));
    test(!is_palindrome("otter"));
    test(!is_palindrome("rente"));
    test(!is_palindrome("tierreiter"));
    test(!is_palindrome("xD"));
    test(!is_palindrome("123432"));
    test(!is_palindrome("1234543"));

    simpletests::test_result();

    auto words = read_file("words.txt");

    for(const auto& word : words)
    {
        std::cout << std::setw(20) << word << " -> ";
        std::cout << std::boolalpha << is_palindrome(word) << std::endl;
    }
}
