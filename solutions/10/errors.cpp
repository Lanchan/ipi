#include <iostream>

int pp()
{
    int* a = new int;
    *a = 5;
    // delete a otherwise memory leak
    delete a;
    // initialize value
    a = new int(0);

    // delete second pointer, too
    int ret = *a;
    delete a;
    return ret;
}

bool is_large_number(int z)
{
    // initialize ret
    bool ret = false;
    if(z > 1000)
    {
        ret = true;
    }
    return ret;
}

int quad_array(int n)
{
    int* z = new int[n];
    for(int i = 0; i < n; ++i)
    {
        z[i] = i*i;
    }
    // array indices are in [0; n-1]
    int ret = z[n - 1];
    delete[] z;
    return ret;
}

int* set_variable(int z)
{
    int* n = new int;
    *n = z;
    // don't delete otherwise pointer to deleted value is returned
    // delete n;
    return n;
}

// don't return pointer
float lv(float f)
{
    f *= 2.0;
    // pointer to local variable returned
    return f;
}

int main()
{
    std::cout << pp() << std::endl;

    std::cout << is_large_number(100) << std::endl;

    std::cout << quad_array(1000) << std::endl;

    int* variable = set_variable(3);
    std::cout << *variable << std::endl;
    delete variable;

    std::cout << lv(3) << std::endl;
}
