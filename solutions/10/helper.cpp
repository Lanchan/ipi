#include <fstream>
#include <iostream>

#include "helper.h"


void remove_lineending(std::string& word)
{
    if(word[word.size() - 1] == '\r')
    {
        word.pop_back();
    }
}

std::vector<std::string> read_file(const std::string& filename)
{
    std::ifstream fin(filename);
    if(!fin.good())
    {
        std::cerr << "Could not open file '" << filename << "'!" << std::endl;
        return {};
    }

    std::string line;
    std::vector<std::string> lines;
    while(std::getline(fin, line))
    {
        remove_lineending(line);
        if(!line.empty())
        {
            lines.push_back(line);
        }
    }
    return lines;
}
