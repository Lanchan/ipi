#include <iostream>
using namespace std;

int euklid(int a, int b)
{
    return b == 0 ? a : euklid(b, a % b);
}

int main()
{
    int a, b;  // die ganzen Zahlen, deren ggT berechnet werden soll
    int ggt;   // Variable zum Speichern des ggT

    a = 123;
    b = 1968;
    std::cout << "Euklid sagt: " << euklid(a, b) << std::endl;
    while(true)
    {
        if(a == 0)
        {
            ggt = b;
            break;
        }
        if(b == 0)
        {
            ggt = a;
            break;
        }
        if(a > b)
        {
            a = a - b;
        }
        else
        {
            b = b - a;
        }

    }

    cout << "Der groesste gemeinsame Teiler ist:  " << ggt << endl;


    return 0;
}
