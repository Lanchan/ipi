// This adds std::cout and std::endl
#include <iostream>

// Now you can use cout and endl without std::
using namespace std;

int main()
{
    const int N = 20;

    for(int lhs = N; lhs >= 1; --lhs)
    {
        for(int rhs = 1; rhs <= N; ++rhs)
        {
            std::cout.width(3);
            std::cout << lhs * rhs << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

