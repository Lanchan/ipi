#include <iostream>
#include <string>


enum class State
{
    Idle = 0,
    One = 1,
    Two = 2,
    Check = 3,
    OutOfOrder = 4
};

enum class Event
{
    InsertOne = 0,
    InsertTwo = 1,
    InsertAny = 2,
    Cancel = 3,
    Full = 4,
    NotFull = 5
};

std::string state_name(State state);
Event read_event();
State transition(State state, Event event);

int main()
{
    State state = State::Idle;

    while(true)
    {
        std::cout << state_name(state) << std::endl;
        Event event = read_event();
        state = transition(state, event);
    }
}

std::string state_name(State state)
{
    switch(state)
    {
    case State::OutOfOrder:
        return "out of order";
    case State::Idle:
        return "idle";
    case State::Check:
        return "checking";
    case State::One:
        return "credit: 1";
    case State::Two:
        return "credit: 2";
    }
}

State transition(State state, Event event)
{
    switch(state)
    {
    case State::Idle:
        switch(event)
        {
        case Event::InsertOne:
            return State::One;
        case Event::InsertTwo:
            return State::Two;
        case Event::InsertAny:
            std::cout << "the machine returns your insertion" << std::endl;
            return State::Two;
        default:
            std::cout << "you cannot do that now" << std::endl;
            return state;
        }

    case State::One:
        switch(event)
        {
        case Event::InsertOne:
            return State::Two;
        case Event::InsertTwo:
            std::cout << "a ticket is printed" << std::endl;
            return State::Check;
        case Event::InsertAny:
            std::cout << "the machine returns your insertion" << std::endl;
            return State::One;
        case Event::Cancel:
            std::cout << "the machine returns your money" << std::endl;
            return State::Idle;
        default:
            std::cout << "you cannot do that now" << std::endl;
            return state;
        }
    case State::Two:
        switch(event)
        {
        case Event::InsertOne:
            std::cout << "a ticket is printed" << std::endl;
            return State::Check;
        case Event::InsertTwo:
            std::cout << "the machine returns 2 moneys" << std::endl;
            return State::Two;
        case Event::InsertAny:
            std::cout << "the machine returns your insertion" << std::endl;
            return State::One;
        case Event::Cancel:
            std::cout << "the machine returns your money" << std::endl;
            return State::Idle;
        default:
            std::cout << "you cannot do that now" << std::endl;
            return state;
        }

    case State::Check:
        switch(event)
        {
        case Event::NotFull:
            return State::Idle;
        case Event::Full:
            std::cout << "the machine displays 'out of order'" << std::endl;
            return State::OutOfOrder;
        default:
            std::cout << "you cannot do that now" << std::endl;
            return state;
        }

    case State::OutOfOrder:
        switch(event)
        {
        case Event::InsertAny:
        case Event::InsertOne:
        case Event::InsertTwo:
        case Event::Cancel:
            std::cout << "the machine display 'out of order'" << std::endl;
            return State::OutOfOrder;
        default:
            std::cout << "you cannot do that now" << std::endl;
            return state;
        }
    }
}

Event read_event()
{
    std::string input;

    while(true)
    {
        std::cout << "enter event: ";
        std::cin >> input;

        if(input == "insert1")
        {
            return Event::InsertOne;
        }
        if(input == "insert2")
        {
            return Event::InsertTwo;
        }
        if(input.substr(0, 6) == "insert")
        {
            return Event::InsertAny;
        }
        if(input == "cancel")
        {
            return Event::Cancel;
        }
        if(input == "full")
        {
            return Event::Full;
        }
        if(input == "notfull")
        {
            return Event::NotFull;
        }
        std::cout << "that is not a valid event" << std::endl;
    }
}