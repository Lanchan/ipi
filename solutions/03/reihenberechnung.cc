#include <iostream>
#include <c++/limits>

using namespace std;

const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

int main()
{
    double summeTeilA, summeTeilB; // Variablen zum Speichern
    int i=1;                       // Laufvariable

    summeTeilA = 0;
    double alteSumme = std::numeric_limits<double>::infinity();
    while(alteSumme != summeTeilA)
    {
        alteSumme = summeTeilA;
        summeTeilA += 1.0 / i / i;
        ++i;
    }

    cout << "Die Summe aus Teil a ergibt:  " << summeTeilA << endl;
    cout << "Abweichung: " << summeTeilA*6 - PI*PI << std::endl;

    summeTeilB = 0;
    alteSumme = std::numeric_limits<double>::infinity();

    i = 1;

    while(alteSumme != summeTeilB)
    {
        alteSumme = summeTeilB;
        summeTeilB -= 1.0 / i / i;
        ++i;
        summeTeilB += 1.0 / i / i;
        ++i;
    }

    // Fuegen Sie hier Ihren Code fuer Aufgabenteil b ein.

    cout << "Die Summe aus Teil b ergibt:  " << summeTeilB << endl;
    cout << "Abweichung: " << summeTeilB*12 + PI*PI << std::endl;

    return 0;
}

