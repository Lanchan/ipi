#include <iostream>
#include <cassert>

#include "image.h"

Image mirror_x(const Image& image)
{
    Image mirrored(image.width() * 2, image.height());
    for(int y = 0; y < image.height(); ++y)
    {
        for(int x = 0; x < image.width(); ++x)
        {
            mirrored(x, y) = image(x, y);
            mirrored(mirrored.width() - 1 - x, y) = image(x, y);
        }
    }
    return mirrored;
}

Image mirror_y(const Image& image)
{
    Image mirrored(image.width(), image.height() * 2);
    for(int y = 0; y < image.height(); ++y)
    {
        for(int x = 0; x < image.width(); ++x)
        {
            mirrored(x, y) = image(x, y);
            mirrored(x, mirrored.height() - 1 - y) = image(x, y);
        }
    }
    return mirrored;
}

Image kaleidoscope4(const Image& image)
{
    return mirror_y(mirror_x(image));
}

Image kaleidoscope8(const Image& image)
{
    Image copy(image);

    int side = std::min(image.width(), image.height());

    for(int y = 0; y < side; ++y)
    {
        for(int x = y + 1; x < side; ++x)
        {
            copy(x, y) = copy(y, x);
        }
    }

    return kaleidoscope4(copy);
}

int main()
{
    Image test(3, 2);
    test(0, 0) = 1; test(1, 0) = 2; test(2, 0) = 3;
    test(0, 1) = 4; test(1, 1) = 5; test(2, 1) = 6;

    Image expected(6, 2);
    expected(0, 0) = 1; expected(1, 0) = 2; expected(2, 0) = 3;
    expected(0, 1) = 4; expected(1, 1) = 5; expected(2, 1) = 6;
    expected(5, 0) = 1; expected(4, 0) = 2; expected(3, 0) = 3;
    expected(5, 1) = 4; expected(4, 1) = 5; expected(3, 1) = 6;

    assert(mirror_x(test) == expected);

    expected.resize(3, 4);
    expected(0, 0) = 1; expected(1, 0) = 2; expected(2, 0) = 3;
    expected(0, 1) = 4; expected(1, 1) = 5; expected(2, 1) = 6;
    expected(0, 3) = 1; expected(1, 3) = 2; expected(2, 3) = 3;
    expected(0, 2) = 4; expected(1, 2) = 5; expected(2, 2) = 6;

    assert(mirror_y(test) == expected);

    Image christmas = readPGM("christmas.pgm");
    writePGM(mirror_x(christmas), "christmas-mirror-x.pgm");
    writePGM(mirror_y(christmas), "christmas-mirror-y.pgm");

    writePGM(kaleidoscope4(christmas), "christmas-4.pgm");
    writePGM(kaleidoscope8(christmas), "christmas-8.pgm");

    Image tux = readPGM("tux.pgm");
    writePGM(kaleidoscope8(tux), "tux-8.pgm");
}
