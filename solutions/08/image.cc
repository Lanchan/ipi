#include <iostream>
#include <iomanip>

#include "image.h"

using uint = unsigned int;

Image chessboard(uint width, uint height, uint square_size)
{
    Image i(width, height);
    for(uint y = 0; y < height; ++y)
    {
        for(uint x = 0; x < width; ++x)
        {
            i(x, y) = (x / square_size + y / square_size) % 2 * 255;
        }
    }
    return i;
}

Image invert_image(const Image& image)
{
    Image copy(image);
    for(int y = 0; y < copy.height(); ++y)
    {
        for(int x = 0; x < copy.width(); ++x)
        {
            copy(x, y) = 255 - copy(x, y);
        }
    }
    return copy;
}

int main()
{
    Image i(4, 3);
    i(0, 0) = 250;
    i(1, 0) = i(0, 1) = 220;
    i(2, 0) = i(1, 1) = i(0, 2) = 190;
    i(3, 0) = i(2, 1) = i(1, 2) = 160;
    i(3, 1) = i(2, 2) = 130;
    i(3, 2) = 100;

    std::cout << "should be:" << std::endl;
    std::cout << "250 220 190 160" << std::endl;
    std::cout << "220 190 160 130" << std::endl;
    std::cout << "190 160 130 100" << std::endl;
    std::cout << std::endl;

    std::cout << "got:" << std::endl;
    std::cout << to_string(i) << std::endl;

    writePGM(i, "test_image_b.pgm");


    for(int y = 0; y < i.height(); ++y)
    {
        for(int x = 0; x < i.width(); ++x)
        {
            i(x, y) = (x + y) % 2 * 255;
        }
    }

    std::cout << "chess:" << std::endl;
    std::cout << to_string(i) << std::endl;

    writePGM(i, "test_image_c.pgm");


    writePGM(i, "board4x3.pgm");
    Image copy = readPGM("board4x3.pgm");

    std::cout << "images are identical: ";
    std::cout << std::boolalpha << (i == copy) << std::endl;


    Image chess = chessboard(4, 3, 1);
    std::cout << "chessboard images are identical: ";
    std::cout << std::boolalpha << (i == chess) << std::endl;

    Image chess_large = chessboard(400, 300, 20);
    writePGM(chess_large, "board400x300.pgm");
    Image chess_large_copy = readPGM("board400x300.pgm");
    std::cout << "large chessboard images are identical: ";
    std::cout << std::boolalpha << (chess_large == chess_large_copy) << std::endl;

    Image chess_inverse = invert_image(chess_large);
    writePGM(chess_inverse, "board400x300-inverse.pgm");

    Image christmas = readPGM("christmas.pgm");
    writePGM(invert_image(christmas), "christmas-inverse.pgm");
}
