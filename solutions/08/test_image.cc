#include <iostream>
#include <cassert>

#include "image.h"

bool all_same(const Image& image, Image::PixelType value)
{
    for(int x = 0; x < image.width(); ++x)
    {
        for(int y = 0; y < image.height(); ++y)
        {
            if(image(x, y) != value)
            {
                return false;
            }
        }
    }
    return true;
}

int main()
{
    Image empty;
    assert(empty.width() == 0);
    assert(empty.height() == 0);
    assert(empty.size() == 0);

    Image square(3, 3);
    assert(square.width() == 3);
    assert(square.height() == 3);
    assert(square.size() == 9);
    assert(all_same(square, 0));

    square(0, 0) = square(0, 1) = square(0, 2) = 1;
    square(1, 0) = square(1, 1) = square(1, 2) = 1;
    square(2, 0) = square(2, 1) = square(2, 2) = 1;
    assert(all_same(square, 1));

    Image rect(3, 4);
    assert(rect.width() == 3);
    assert(rect.height() == 4);
    assert(rect.size() == 12);
    assert(all_same(rect, 0));

    rect(0, 0) = rect(0, 1) = rect(0, 2) = rect(0, 3) = 2;
    rect(1, 0) = rect(1, 1) = rect(1, 2) = rect(1, 3) = 2;
    rect(2, 0) = rect(2, 1) = rect(2, 2) = rect(2, 3) = 2;
    assert(all_same(rect, 2));

    rect.resize(4, 3);
    assert(rect.width() == 4);
    assert(rect.height() == 3);
    assert(rect.size() == 12);
    assert(all_same(rect, 2));

    empty.resize(2, 3);
    assert(empty.width() == 2);
    assert(empty.height() == 3);
    assert(empty.size() == 6);
    assert(all_same(empty, 0));

    square.resize(2, 2);
    assert(square.width() == 2);
    assert(square.height() == 2);
    assert(square.size() == 4);
    assert(all_same(square, 1));

    assert(!(square == empty));
    assert(square == square);

    Image copy(square);
    assert(square == copy);
    copy(1, 1) = 10;
    assert(!(square == copy));

    copy = rect;
    assert(copy == rect);
    copy.resize(3, 4);
    assert(!(copy == rect));

    std::string with_spaces = "1 1 \n1 1 \n";
    std::string without_spaces = "1 1\n1 1\n";
    std::string im_string = to_string(square);
    assert(im_string == with_spaces ||
            im_string == without_spaces);

    square(0, 1) = 10;
    with_spaces = "1 1 \n10 1 \n";
    without_spaces = "1 1\n10 1\n";
    im_string = to_string(square);
    assert(im_string == with_spaces ||
            im_string == without_spaces);

    rect.resize(2, 3);
    rect(0, 0) = 1;
    rect(1, 0) = 127;
    with_spaces = "1 127 \n2 2 \n2 2 \n";
    without_spaces = "1 127\n2 2\n2 2\n";
    im_string = to_string(rect);
    assert(im_string == with_spaces ||
            im_string == without_spaces);

    writePGM(rect, "test_image.pgm");
    Image read = readPGM("test_image.pgm");

    assert(read == rect);

    std::cout << "all tests passed" << std::endl;
}
