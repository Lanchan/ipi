#include <iostream>
#include <string>
using namespace std;

class Room {
private:
    int allowedEntries;
    Room* North;
    Room* South;
    Room* East;
    Room* West;
protected:
    string name;
public:
    Room(string name, int allowedEntries);

    Room* north();
    Room* south();
    Room* east();
    Room* west();

    void connect_north(Room* destination);
    void connect_south(Room* destination);
    void connect_east(Room* destination);
    void connect_west(Room* destination);

    virtual bool game_over() const;
    virtual void enter();
    // for inheritance we always want a virtual destructor
    virtual ~Room() = default;
};

Room::Room(string name, int allowedEntries)
    : allowedEntries(allowedEntries),
      North(this), South(this), East(this), West(this),
      name(name)
{}

Room* Room::north()
{
    return North;
}

Room* Room::south()
{
    return South;
}

Room* Room::east()
{
    return East;
}

Room* Room::west()
{
    return West;
}

void Room::connect_north(Room* destination)
{
    North = destination;
    destination->South = this;
}

void Room::connect_south(Room* destination)
{
    South = destination;
    destination->North = this;
}

void Room::connect_east(Room* destination)
{
    East = destination;
    destination->West = this;
}

void Room::connect_west(Room* destination)
{
    West = destination;
    destination->East = this;
}

void Room::enter()
{
    --allowedEntries;
    if(allowedEntries < 0)
    {
        cout << "Sie haben diesen Raum zu oft betreten. Game Over" << endl;
        return;
    }
    cout << "Sie sind hier: " << this->name << ". Es gibt Wege nach";
    if (this->East != this) cout << " O";
    if (this->West != this) cout << " W";
    if (this->North != this) cout << " N";
    if (this->South != this) cout << " S";
    cout << endl << "Dieser Raum darf noch " << allowedEntries;
    cout << " mal betreten werden" << endl;
}

bool Room::game_over() const
{
    return allowedEntries < 0;
}

class Exit : public Room
{
public:
    Exit();
    bool game_over() const override;
    void enter() override;
};

Exit::Exit()
    : Room("Ausgang", 1)
{}

void Exit::enter()
{
    cout << "Sie sind hier: " << this->name << ". Sie haben gewonnen" << endl;
}

bool Exit::game_over() const
{
    return true;
}

Room* choose_action(Room* here)
{
    cout << "Wohin? (X:exit)" << endl;
    string in;
    cin >> in;
    switch (toupper(in[0])) {
    case 'N':
        return here->north();
    case 'S':
        return here->south();
    case 'O':
        return here->east();
    case 'W':
        return here->west();
    default:
        cout << "Tschuess!\n";
        return nullptr;
    }
}

int main()
{
    Room* r1 = new Room("Bad", 3);
    Room* r2 = new Room("Schlafzimmer", 3);
    Room* r3 = new Room("Wohnzimmer", 3);
    Room* r4 = new Room("Esszimmer", 3);
    Room* r5 = new Room("Flur", 3);
    // new room "Kuech"
    Room* r6 = new Room("Kueche", 3);
    // exit
    Room* e = new Exit;

    r1->connect_north(r2);
    r1->connect_east(r5);
    r2->connect_east(r3);
    r3->connect_south(r5);
    r3->connect_east(r4);
    r4->connect_south(r6);
    r5->connect_east(r6);
    r5->connect_south(e);

    // starting in "Schlafzimmer"
    Room* here = r2;
    do {
        here->enter();
        if(here->game_over())
        {
            break;
        }

    } while ((here = choose_action(here)));

    delete r1;
    delete r2;
    delete r3;
    delete r4;
    delete r5;
    delete r6;
    delete e;
}
