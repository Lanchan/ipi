#pragma once

#include "sphere.h"

// Inheritance here does not make sense, since a Circle is no a Sphere
class Circle : public Sphere
{
public:
    double getArea() const;
    double getCircumference() const;
};
