#include "cube.h"


Cube::Cube(double side)
    : base(side)
{}

void Cube::setSide(double side)
{
    base.setWidth(side);
}

double Cube::getSide() const
{
    return base.getWidth();
}

double Cube::getVolume() const
{
    return base.getArea() * getSide();
}

double Cube::getSurfaceArea() const
{
    return base.getArea() * 6;
}
