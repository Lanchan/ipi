#include <iostream>
#include <cmath>
using namespace std;

class Point2D {
// we want to change this so we can access x,y in Point3D
protected:
  double x, y;

public:
  Point2D();
  Point2D(double x, double y);

  void setX(double x);
  void setY(double y);

  double getX() const;
  double getY() const;

  double calcDistance(const Point2D& point) const;
};

std::ostream& operator<<(std::ostream& out, const Point2D& point)
{
    return out << "(" << point.getX() << ", " << point.getY() << ")";
}

Point2D::Point2D()
    : x(0), y(0)
{}

Point2D::Point2D(double x, double y)
    : x(x), y(y)
{}

void Point2D::setX(double x)
{
    this->x = x;
}

void Point2D::setY(double y)
{
    this->y = y;
}

double Point2D::getX() const
{
    return x;
}

double Point2D::getY() const
{
    return y;
}

double Point2D::calcDistance(const Point2D& point) const
{
    return std::sqrt(std::pow(x - point.x, 2) + std::pow(y - point.y, 2));
}

class Point3D : public Point2D
{
private:
    double z;

public:
    Point3D();
    Point3D(double x, double y, double z);

    void setZ(double z);
    double getZ() const;

    double calcDistance(const Point3D& point) const;
};

std::ostream& operator<<(std::ostream& out, const Point3D& point)
{
    out << "(" << point.getX() << ", " << point.getY();
    out << ", " << point.getZ() << ")";
    return out;
}

Point3D::Point3D()
    : Point2D(), z(0)
{}

Point3D::Point3D(double x, double y, double z)
    : Point2D(x, y), z(z)
{}

void Point3D::setZ(double z)
{
    this->z = z;
}

double Point3D::getZ() const
{
    return z;
}

double Point3D::calcDistance(const Point3D& point) const
{
    return std::sqrt(std::pow(x - point.x, 2) +
                     std::pow(y - point.y, 2) +
                     std::pow(z - point.z, 2));
}

int main()
{
    Point2D p1(2, 5);
    Point2D p2(3, -3);

    std::cout << "distance from " << p1 << " to " << p2 << ": ";
    std::cout << p1.calcDistance(p2) << std::endl;

    Point3D p3;
    p3.setX(2);
    p3.setY(5);
    p3.setZ(4);
    Point3D p4(3, -3, 0);

    std::cout << "distance from " << p3 << " to " << p4 << ": ";
    std::cout << p3.calcDistance(p4) << std::endl;

	return 0;
}
