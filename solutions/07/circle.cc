#include "circle.h"

const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

double Circle::getArea() const
{
    return PI * radius * radius;
}

double Circle::getCircumference() const
{
    return PI * radius * 2;
}
