#pragma once

#include "rectangle.h"

class Square : public Rectangle
{
public:
    Square() = default;
    Square(double side);

    // these should also be overridden
    void setWidth(double width) override;
    void setHeight(double width) override;
};
