#pragma once

#include "square.h"

class Cube
{
private:
    Square base;

public:
    Cube() = default;
    Cube(double side);
    void setSide(double side);
    double getSide() const;
    double getVolume() const;
    double getSurfaceArea() const;
};
