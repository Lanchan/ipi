#include "sphere.h"

const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

Sphere::Sphere(double radius)
    : radius(radius)
{}

void Sphere::setRadius(double radius)
{
    this->radius = radius;
}

double Sphere::getRadius() const
{
    return radius;
}

double Sphere::getVolume() const
{
    return 4/3.0 * PI * radius * radius * radius;
}

double Sphere::getSurfaceArea() const
{
    return 4 * PI * radius * radius;
}
