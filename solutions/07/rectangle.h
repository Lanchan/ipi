#pragma once

class Rectangle
{
private:
    double width = 0;
    double height = 0;

public:
    Rectangle() = default;
    Rectangle(double width, double height);
    double getWidth() const;
    double getHeight() const;
    virtual void setWidth(double width);
    virtual void setHeight(double height);
    double getArea() const;
    double getCircumference() const;
};
