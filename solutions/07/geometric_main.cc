#include <cassert>
#include <iostream>

#include "rectangle.h"
#include "square.h"
#include "cube.h"
#include "sphere.h"


int main()
{
    Rectangle r;
    assert(r.getWidth() == 0);
    assert(r.getHeight() == 0);
    assert(r.getArea() == 0);
    assert(r.getCircumference() == 0);

    r.setWidth(2);
    assert(r.getWidth() == 2);
    assert(r.getHeight() == 0);
    assert(r.getArea() == 0);
    assert(r.getCircumference() == 4);

    r.setHeight(3);
    assert(r.getWidth() == 2);
    assert(r.getHeight() == 3);
    assert(r.getArea() == 6);
    assert(r.getCircumference() == 10);

    Rectangle r2(2, 3);
    assert(r2.getWidth() == r.getWidth());
    assert(r2.getHeight() == r.getHeight());

    Square s;
    assert(s.getWidth() == 0);
    assert(s.getHeight() == 0);

    Square s2(4);
    assert(s2.getWidth() == 4);
    assert(s2.getHeight() == 4);

    s2.setWidth(1);
    assert(s2.getWidth() == 1);
    assert(s2.getHeight() == 1);

    s2.setHeight(2);
    assert(s2.getWidth() == 2);
    assert(s2.getHeight() == 2);

    Sphere sp(0);
    assert(sp.getRadius() == 0);
    sp.setRadius(2);

    Sphere sp2(2);
    assert(sp2.getRadius() == sp.getRadius());

    Cube cu;
    assert(cu.getSide() == 0);
    assert(cu.getVolume() == 0);
    assert(cu.getSurfaceArea() == 0);

    cu.setSide(1);
    assert(cu.getSide() == 1);
    assert(cu.getVolume() == 1);
    assert(cu.getSurfaceArea() == 6);

    std::cout << "all tests passed" << std::endl;
}
