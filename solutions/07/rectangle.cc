#include "rectangle.h"

Rectangle::Rectangle(double width, double height)
    : width(width), height(height)
{}

double Rectangle::getWidth() const
{
    return width;
}

double Rectangle::getHeight() const
{
    return height;
}

void Rectangle::setWidth(double width)
{
    this->width = width;
}

void Rectangle::setHeight(double height)
{
    this->height = height;
}

double Rectangle::getArea() const
{
    return width * height;
}

double Rectangle::getCircumference() const
{
    return width*2 + height*2;
}
