#include "square.h"


Square::Square(double side)
    : Rectangle(side, side)
{}

void Square::setWidth(double width)
{
    Rectangle::setWidth(width);
    Rectangle::setHeight(width);
}

void Square::setHeight(double width)
{
    Rectangle::setWidth(width);
    Rectangle::setHeight(width);
}
