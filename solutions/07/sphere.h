#pragma once


class Sphere
{
protected:
    double radius;

public:
    Sphere() = default;
    Sphere(double radius);

    void setRadius(double radius);
    double getRadius() const;
    double getVolume() const;
    double getSurfaceArea() const;
};
