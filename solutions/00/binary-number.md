Binary numbers
==============

### Notation:

- `110`, `199` are numbers in base `10`
- `0b110`, `0b10101101` are numbers in base `2`
- `0x110`, `0xffffcc00` are numbers in base `16`

### a)

```
    0b110 = (0 * 2^0) + (1 * 2^1) + (1 * 2^2)
          = 0*1 + 1*2 + 1*4
          = 0 + 2 + 4
          = 6

    0b10101101 = 1*1 + 0*2 + 1*4 + 1*8 + 0*16 + 1*32 + 0*64 + 1*128
               = 1 + 4 + 8 + 32 + 128
               = 173
```

### c)

```
	23 % 2 = 	  1 (last digit)
	23 / 2 = 11

	11 % 2 =      1
	11 / 2 = 5

	5 % 2 =       1
	5 / 2 = 2

	2 % 2 =       0
	2 / 2 = 1

	1 % 2 =       1 (first digit)
	1 / 2 = 0 (done)

	23 = 0b10111
```

Alternatively find largest number that is a power of `2` that is not larger than `23` => `16 = 2^4`

```
	23 / 16 =     1 (first digit)
	23 % 16 = 7

	7 / 8   =     0
	7 % 8   = 7

	7 / 4   =     1
	7 % 4   = 3

	3 / 2   =     1
	3 % 2   = 1

	1 / 1   =     1 (done)

	23 = 0b10111
```

```
	44 / 32 =    1 (first digit)
	44 % 32 = 12

	12 / 16 =    0
	12 % 16 = 12

	12 / 8  =    1
	12 % 8  = 4

	4 / 4   =    1
	4 % 4   = 0		(not done yet)

	0 / 2   =    0

	0 / 1   =    0  (done)

	44 = 0b101100
```