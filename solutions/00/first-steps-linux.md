First Steps (Linux)
===================

### a)

- `CTRL+ALT+T` or find "terminal" in launcher

```sh

	pwd
	# /home/user

	mkdir food
	mkdir drinks
	pwd
	# /home/user

	cd food
	pwd
	# /home/user/food
	mkdir breakfast

	cd breakfast
	pwd
	# /home/user/food/breakfast
	mkdir toast
	mkdir eggs

	cd ..
	pwd
	# /home/user/food
	mkdir lunch

	cd ..
	pwd
	# /home/user
	mkdir food/dinner/bread
	# error
	mkdir -p food/dinner/bread

```

### b)

```sh

	cd /
	ls
	# bin etc lib64 proc srv var boot home ...

	cd
	pwd
	# /home/user
```

### c)

Possible text editors are:

- gedit (gui, easy to use)
- kate (gui easy to use)
- vi
- vim
- nano
- emacs


```sh

	gedit
	# terminal is locked
	# commands are not accepted

	gedit&
	# terminal works as expected

	gedit lemonade.txt&
	gedit tea.txt coffee.txt&
	# write some text and save

	ls
	# ... coffee.txt food lemonade.txt tea.txt

	ls -l
	# -rw-rw-r-- 1 user user   15 Okt 20 16:19 coffee.txt
	# ...
	# drwxrwxr-x 1 user user 4096 Okt 20 16:18 food
	# ...
	# -rw-rw-r-- 1 user user  109 Okt 20 16:19 lemonade.txt
	# ...
	# -rw-rw-r-- 1 user user   31 Okt 20 16:19 tea.txt

	man ls
	# displays man page
	# exit with 'Q'
```

Some common usecases are

- `ls` simply lists visible files/directories one after another
- `ls -a` includes **A**ll (including hidden) files (starting with `.`)
- `ls -l` show a **L**ong table with meta information about each file
- `ls -lh` displays the file size in **H**uman readable format
- `ls -lah` all options combined

### d)

```sh

	man pwd > file.txt
	# file.txt now contains the manpage for pwd

	man ls > file.txt
	# > overwrites
	# file.txt now contains only the manpage for ls

	man mkdir >> file.txt
	# >> appends
	# file.txt now contains both the manpage for ls and mkdir
```

### e)

```sh

	cp lemonade.txt drinks
	ls
	# lemonade.txt is still present
	ls drinks
	# lemonade.txt is also here

	mv tea.txt drinks
	ls
	# tea.txt is gone
	ls drinks
	# tea.txt is here
```


### f)

```sh
	
	rm coffee.txt
	ls 
	# coffee.txt is gone

	rm drinks
	# cannot rm a directory

	rmdir drinks
	# cannot rmdir non-empty directory

	rm drinks/lemonade.txt drinks/tea.txt
	rmdir drinks
```