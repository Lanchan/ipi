First Steps (Windows)
=====================

### a)

- `Win+R` -> enter `cmd`
- type `cmd` into folder path from explorer

```cmd

	cd
	# C:\Users\user

	mkdir food
	mkdir drinks
	cd
	# C:\Users\user

	cd food
	cd
	# C:\Users\user\food
	mkdir breakfast

	cd breakfast
	cd
	# C:\Users\user\food\breakfast
	mkdir toast
	mkdir eggs

	cd ..
	cd
	# C:\Users\user\food
	mkdir lunch

	cd ..
	cd
	# C:\Users\user
	mkdir food\dinner\bread

```

### b)

```cmd

	cd /
	dir
	# Program Files, Program Files (x86), Users, Windows

	cd Users\user
```

### c)

Possible text editors are (links are in README.md):

- Notepad++ (free, open source)
- Sublime Text 3 (unlimited trial)

Please don't use the default notepad


```cmd

	start notepad++
	# terminal is not locked (different from linux)
	# syntax with & is not needed
	
	start notepad++ lemonade.txt
	# notepad++ asks to create lemonade.txt
	start notepad++ tea.txt coffee.txt
	# notepad++ asks to create tea.txt and coffee.txt
	
	dir
	# prints the following columns:
	# date, time, folder, size, name
```

### d)

```cmd

	mkdir /? > file.txt
	# file.txt now contains the help message for mkdir

	rmdir /? > file.txt
	# > overwrites
	# file.txt now contains only the help message for rmdir

	mkdir >> file.txt
	# >> appends
	# file.txt now contains both the help message for mkdir and rmdir
```

### e)

```cmd

	copy lemonade.txt drinks
	dir
	# lemonade.txt is still present
	dir drinks
	# lemonade.txt is also here

	move tea.txt drinks
	dir
	# tea.txt is gone
	dir drinks
	# tea.txt is here
```


### f)

```cmd
	
	del coffee.txt
	dir
	# coffee.txt is gone

	del drinks
	# choose Y/N to delete everything inside drinks/
	# if Y then all files in drinks are deleted
	# else nothing is done

	rmdir drinks
	# cannot rmdir non-empty directory

	del drinks\lemonade.txt drinks\tea.txt
	rmdir drinks
```
