Compiler
========

### a)

```sh
	
	g++ aufgabe3.cc
	ls
	# a.out aufgabe3.cc

	./a.out
	# 5587
	# 4

	g++ -o aufgabe3 aufgabe3.cc
	ls
	# a.out aufgabe3 aufgabe3.cc
```

### c)

Removing semi colon in line 5

```sh

	g++ -Wall aufgabe3.cc
	# error in line 6!
```

### d)

Replace `37` with `0` in line 8

```sh

	g++ aufgabe3.cc
	# no warning
	./a.out
	# Crash: Floating point exception (core dumped)

	g++ -Wall aufgabe3.cc
	# warning: division by zero in line 8
```

### e)

Replace `i` with `j` in line 5

```sh

	g++ aufgabe3.cc
	# error: i was not declared in line 6!

	g++ -Wall aufgabe3.cc
	# warning: unused variable j in line 5
	# + same error
```

### f)

Delete line 10

```h

	g++ aufgabe3.cc
	./a.out
	# everything ok

	g++ aufgabe3.cc -Wall
	./a.out
	# everything ok
```