#include <limits>
#include <vector>
#include <cassert>
#include <utility>
#include <functional>
#include <algorithm>

#include "image.h"
#include "rectangle.h"

using Rectangles = std::vector<Rectangle>;

struct BestIndex
{
    size_t free_index = 0;
    size_t gift_index = 0;
    bool need_transpose = false;
    double score = std::numeric_limits<double>::infinity();

    bool no_match() const
    {
        return score == std::numeric_limits<double>::infinity();
    }
};

std::ostream& operator<<(std::ostream& out, const Rectangle& rect)
{
    out << "[" << rect.getX0() << ":" << rect.getX1() << "] ";
    out << "[" << rect.getY0() << ":" << rect.getY1() << "]";
    return out;
}

double bssf_score(const Rectangle& free, const Rectangle& obj)
{
    double width = free.width() - obj.width();
    double height = free.height() - obj.height();

    if(width < 0 || height < 0)
    {
        return std::numeric_limits<double>::infinity();
    }

    return std::min(width, height);
}

Rectangles sas_rule(const Rectangle& free, const Rectangle& best)
{
    //          a       b
    // +--------+-------+
    // |        |       |
    // |        |       |
    // |        |       |
    // +--------+-------+ d
    // c        |       |
    //          |       |
    //          |       |
    //          +-------+
    //          e

    Point c = {
        free.getX0(),
        free.getY0() + best.height()
    };
    Point b = free.getP1();
    Point e = {
        free.getX0() + best.width(),
        free.getY0()
    };

    if(free.width() < free.height())
    {
        Point d = {
            free.getX1(),
            free.getY0() + best.height()
        };
        return {
            {c, b},
            {e, d},
        };
    }
    else
    {
        Point a = {
            free.getX0() + best.width(),
            free.getY1()
        };
        return {
            {c, a},
            {e, b}
        };
    }
}

Rectangle remove(Rectangles& rects, size_t index)
{
    Rectangle rect = rects.at(index);
    rects.erase(rects.begin() + index);

    return rect;
}

void draw_rect(Image& canvas, const Rectangle& rect, Color outline, Color fill)
{
    for(int y = rect.getY0() * 5; y < rect.getY1() * 5; ++y)
    {
        for(int x = rect.getX0() * 5; x < rect.getX1() * 5; ++x)
        {
            canvas(x, y) = fill;
        }
    }

    for(int y = rect.getY0() * 5; y < rect.getY1() * 5; ++y)
    {
        canvas(rect.getX0() * 5, y) = canvas(rect.getX1() * 5 - 1, y) = outline;
    }

    for(int x = rect.getX0() * 5; x < rect.getX1() * 5; ++x)
    {
        canvas(x, rect.getY0() * 5) = canvas(x, rect.getY1() * 5 - 1) = outline;
    }
}

BestIndex find_best_index(const Rectangles& to_be_placed, const Rectangles& free_rectangles)
{
    BestIndex best;

    for(unsigned int gift_index = 0; gift_index < to_be_placed.size(); ++gift_index)
    {
        for(unsigned int free_index = 0; free_index < free_rectangles.size(); ++free_index)
        {
            const Rectangle& free = free_rectangles.at(free_index);
            const Rectangle& gift = to_be_placed.at(gift_index);

            double score = bssf_score(free, gift);
            if(score < best.score)
            {
                best.score = score;
                best.need_transpose = false;
                best.gift_index = gift_index;
                best.free_index = free_index;
            }

            score = bssf_score(free, gift.transpose());
            if(score < best.score)
            {
                best.score = score;
                best.need_transpose = true;
                best.gift_index = gift_index;
                best.free_index = free_index;
            }
        }
    }

    return best;
}

double short_side(const Rectangle& rect)
{
    return std::min(rect.width(), rect.height());
}

double long_side(const Rectangle& rect)
{
    return std::max(rect.width(), rect.height());
}

bool asca(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.area() < rhs.area();
}

bool ascss(const Rectangle& lhs, const Rectangle& rhs)
{
    if(short_side(lhs) == short_side(rhs))
    {
        return long_side(lhs) < long_side(rhs);
    }
    return short_side(lhs) < short_side(rhs);
}

bool ascls(const Rectangle& lhs, const Rectangle& rhs)
{
    if(long_side(lhs) == long_side(rhs))
    {
        return short_side(lhs) < short_side(rhs);
    }
    return long_side(lhs) < long_side(rhs);
}

bool ascperim(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.width() + lhs.height() < rhs.width() + rhs.height();
}

bool ascdiff(const Rectangle& lhs, const Rectangle& rhs)
{
    return std::abs(lhs.width() - lhs.height()) < std::abs(rhs.width() - rhs.height());
}

bool ascratio(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.width() / lhs.height() < rhs.width() / rhs.height();
}

bool desca(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.area() > rhs.area();
}

bool descss(const Rectangle& lhs, const Rectangle& rhs)
{
    if(short_side(lhs) == short_side(rhs))
    {
        return long_side(lhs) > long_side(rhs);
    }
    return short_side(lhs) > short_side(rhs);
}

bool descls(const Rectangle& lhs, const Rectangle& rhs)
{
    if(long_side(lhs) == long_side(rhs))
    {
        return short_side(lhs) > short_side(rhs);
    }
    return long_side(lhs) > long_side(rhs);
}

bool descperim(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.width() + lhs.height() > rhs.width() + rhs.height();
}

bool descdiff(const Rectangle& lhs, const Rectangle& rhs)
{
    return std::abs(lhs.width() - lhs.height()) > std::abs(rhs.width() - rhs.height());
}

bool descratio(const Rectangle& lhs, const Rectangle& rhs)
{
    return lhs.width() / lhs.height() > rhs.width() / rhs.height();
}

bool dont_sort(const Rectangle&, const Rectangle&)
{
    return false;
}

void place_gift(Rectangles& to_be_placed, Rectangles& free_rectangles, Rectangles& already_placed,
                const BestIndex& index)
{
    Rectangle gift = remove(to_be_placed, index.gift_index);
    Rectangle free = remove(free_rectangles, index.free_index);

    if(index.need_transpose)
    {
        gift = gift.transpose();
    }
    gift = gift.translate(free.getP0());
    already_placed.push_back(gift);

    auto new_frees = sas_rule(free, gift);
    for(const auto& new_free : new_frees)
    {
        free_rectangles.push_back(new_free);
    }
}

void help_exit(char* name)
{
    std::cerr << "help: " << name << " SORT" << std::endl;
    std::cerr << "    SORT: none     -> don't sort" << std::endl;
    std::cerr << "         ascending:" << std::endl;
    std::cerr << "          asca     -> ascending area" << std::endl;
    std::cerr << "          ascss    -> ascending short side" << std::endl;
    std::cerr << "          ascls    -> ascending long side" << std::endl;
    std::cerr << "          ascperim -> ascending perimeter" << std::endl;
    std::cerr << "          ascdiff  -> ascending side difference" << std::endl;
    std::cerr << "          ascratio -> ascending side ratio" << std::endl;
    std::cerr << "         descending:" << std::endl;
    std::cerr << "          desca ..." << std::endl;
    std::cerr << "          ..." << std::endl;
    exit(1);
}

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        help_exit(argv[0]);
    }

    std::function<bool(const Rectangle&, const Rectangle&)> comparator = dont_sort;
    std::string sort_name = argv[1];

    if(sort_name == "asca")
    {
        comparator = asca;
    }
    else if(sort_name == "desca")
    {
        comparator = desca;
    }
    else if(sort_name == "ascss")
    {
        comparator = ascss;
    }
    else if(sort_name == "descss")
    {
        comparator = descss;
    }
    else if(sort_name == "ascls")
    {
        comparator = ascls;
    }
    else if(sort_name == "descls")
    {
        comparator = ascls;
    }
    else if(sort_name == "ascperim")
    {
        comparator = ascperim;
    }
    else if(sort_name == "descperim")
    {
        comparator = descperim;
    }
    else if(sort_name == "ascdiff")
    {
        comparator = ascdiff;
    }
    else if(sort_name == "descdiff")
    {
        comparator = descdiff;
    }
    else if(sort_name == "ascratio")
    {
        comparator = ascratio;
    }
    else if(sort_name == "descratio")
    {
        comparator = descratio;
    }
    else if(sort_name != "none")
    {
        std::cerr << "unknown sort name" << std::endl;
        help_exit(argv[0]);
    }

    Rectangle table(Point(100.0, 80.0));
    Rectangle tree_stand(Point(30.0, 30.0));

    Rectangles gifts = {
        Rectangle(Point(20.0, 10.0)),
        Rectangle(Point(10.0, 11.0)),
        Rectangle(Point(3.0, 46.0)),
        Rectangle(Point(3.0, 4.0)),
        Rectangle(Point(6.0, 16.0)),
        Rectangle(Point(10.0, 20.0)),
        Rectangle(Point(20.0, 8.0)),
        Rectangle(Point(12.0, 37.0)),
        Rectangle(Point(11.0, 15.0)),
        Rectangle(Point(40.0, 63.0)),
        Rectangle(Point(23.0, 6.0)),
        Rectangle(Point(16.0, 12.0)),
        Rectangle(Point(25.0, 20.0)),
        Rectangle(Point(67.0, 3.0)),
        Rectangle(Point(31.0, 29.0)),
        Rectangle(Point(12.0, 11.0)),
        Rectangle(Point(8.0, 9.0)),
        Rectangle(Point(3.0, 8.0)),
        Rectangle(Point(21.0, 13.0)),
        Rectangle(Point(46.0, 13.0)),
        Rectangle(Point(11.0, 75.0)),
        Rectangle(Point(4.0, 3.0)),
        Rectangle(Point(19.0, 7.0)),
        Rectangle(Point(33.0, 7.0)),
        Rectangle(Point(6.0, 16.0)),
        Rectangle(Point(21.0, 4.0)),
        Rectangle(Point(8.0, 8.0)),
        Rectangle(Point(3.0, 86.0)),
        Rectangle(Point(20.0, 6.0)),
        Rectangle(Point(21.0, 3.0)),
        Rectangle(Point(13.0, 59.0)),
        Rectangle(Point(4.0, 20.0))
    };

    Rectangles to_be_placed(gifts);
    Rectangles already_placed;
    Rectangles free_rectangles{table};

    std::sort(to_be_placed.begin(), to_be_placed.end(), comparator);

    to_be_placed.push_back(tree_stand);
    size_t total_gifts = to_be_placed.size();

    BestIndex stand_index {
        0, to_be_placed.size() - 1, 0, 0
    };
    place_gift(to_be_placed, free_rectangles, already_placed, stand_index);

    while(!to_be_placed.empty())
    {
        BestIndex index = find_best_index(to_be_placed, free_rectangles);

        if(index.no_match())
        {
            break;
        }

        place_gift(to_be_placed, free_rectangles, already_placed, index);
    }

    double total_area = 0;

    for(const Rectangle& lhs : already_placed)
    {
        total_area += lhs.area();
        for(const Rectangle& rhs : already_placed)
        {
            if(&lhs != &rhs)
            {
                Rectangle intersection = rectangle_intersection(lhs, rhs);
                assert(intersection.area() <= 0);
            }
        }
    }

    std::cout << "placed: " << already_placed.size() << "/" << total_gifts << " (" << 100.0 * already_placed.size() / total_gifts << "%)" << std::endl;
    std::cout << "area: " << total_area << "/" << table.area() << " (" << 100 * total_area / table.area() << "%)" << std::endl;

    Image image(table.width() * 5 + 5, table.height() * 5 + 5);

    draw_rect(image, table, {}, {255, 255, 255});
    for(const auto& gift : already_placed)
    {
        draw_rect(image, gift, {0, 0, 0}, {200, 0, 0});
    }
    draw_rect(image, already_placed[0], {0, 0, 0}, {0, 0, 200});
    for(const auto& free : free_rectangles)
    {
        draw_rect(image, free, {0, 0, 0}, {0, 200, 0});
    }

    writePGM(image, "gifts.pgm");
}
