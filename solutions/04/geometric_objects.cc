#include <iostream>

const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

// 2D
double circumference_rectangle(double a, double b)
{
    return a + a + b + b;
}

double area_rectangle(double a, double b)
{
    return a * b;
}

double circumference_square(double a)
{
    return circumference_rectangle(a, a);
}

double area_square(double a)
{
    return area_rectangle(a, a);
}

double circumference_circle(double r)
{
    return 2 * PI * r;
}

double area_circle(double r)
{
    return PI * r * r;
}

// 3D
double surface_area_cuboid(double a, double b, double c)
{
    return 2 * area_rectangle(a, b) +
           2 * area_rectangle(b, c) +
           2 * area_rectangle(a, c);
}

double volume_cuboid(double a, double b, double c)
{
    return a * b * c;
}

double surface_area_cube(double a)
{
    return surface_area_cuboid(a, a, a);
}

double volume_cube(double a)
{
    return volume_cuboid(a, a, a);
}

double surface_area_sphere(double r)
{
    return 4 * PI * r * r;
}

double volume_shpere(double r)
{
    return 4 * PI * r * r * r / 3;
}

int choice_2d();
int choice_3d();

int main()
{
    int dimension;

    std::cout << "Geben Sie die Dimension ein: ";
    std::cin >> dimension;

    switch(dimension)
    {
    case 2:
        return choice_2d();
    case 3:
        return choice_3d();
    default:
        std::cerr << "Diese Eingabe ist nicht erlaubt!" << std::endl;
        return 1;
    }
}

int choice_2d()
{
    std::cout << "Welches Objekt moechten Sie betrachten?" << std::endl;
    std::cout << "0 - Rechteck" << std::endl;
    std::cout << "1 - Quadrat" << std::endl;
    std::cout << "2 - Kreis" << std::endl;
    std::cout << "Eingabe: ";
    int object;
    std::cin >> object;

    switch(object)
    {
    case 0:
        double a;
        double b;

        std::cout << "Geben Sie die Seitenlaengen des Rechtecks ein: ";
        std::cin >> a >> b;
        std::cout << "Das Rechteck hat einen Umfang von ";
        std::cout << circumference_rectangle(a, b);
        std::cout << " und eine Flaeche von ";
        std::cout << area_rectangle(a, b);
        std::cout << std::endl;
        break;
    case 1:
        double s;

        std::cout << "Geben Sie die Seitenlaenge des Quadrats ein: ";
        std::cin >> s;
        std::cout << "Das Quadrat hat einen Umfang von ";
        std::cout << circumference_square(s);
        std::cout << " und eine Flaeche von ";
        std::cout << area_square(s);
        std::cout << std::endl;
        break;
    case 2:
        double r;

        std::cout << "Geben Sie den Radius des Kreises ein: ";
        std::cin >> r;
        std::cout << "Das Rechteck hat einen Umfang von ";
        std::cout << circumference_circle(r);
        std::cout << " und eine Flaeche von ";
        std::cout << area_circle(r);
        std::cout << std::endl;
        break;
    default:
        std::cerr << "Diese Eingabe ist nicht erlaubt!" << std::endl;
        return 1;
    }
    return 0;
}

int choice_3d()
{
    std::cout << "Welches Objekt moechten Sie betrachten?" << std::endl;
    std::cout << "0 - Quader" << std::endl;
    std::cout << "1 - Wuerfel" << std::endl;
    std::cout << "2 - Kugel" << std::endl;
    std::cout << "Eingabe: ";
    int object;
    std::cin >> object;

    switch(object)
    {
    case 0:
        double a;
        double b;
        double c;

        std::cout << "Geben Sie die Seitenlaengen des Quaders ein: ";
        std::cin >> a >> b >> c;
        std::cout << "Der Quader hat eine Oberflache von ";
        std::cout << surface_area_cuboid(a, b, c);
        std::cout << " und ein Volumen von ";
        std::cout << volume_cuboid(a, b, c);
        std::cout << std::endl;
        break;
    case 1:
        double s;

        std::cout << "Geben Sie die Seitenlaengen des Wuefels ein: ";
        std::cin >> s;
        std::cout << "Der Wuerfel hat eine Oberflache von ";
        std::cout << surface_area_cube(s);
        std::cout << " und ein Volumen von ";
        std::cout << volume_cube(s);
        std::cout << std::endl;
        break;
    case 2:
        double r;

        std::cout << "Geben Sie die Seitenlaengen der Kugel ein: ";
        std::cin >> r;
        std::cout << "Die Kugel hat eine Oberflache von ";
        std::cout << surface_area_sphere(r);
        std::cout << " und ein Volumen von ";
        std::cout << volume_shpere(r);
        std::cout << std::endl;
        break;
    default:
        std::cerr << "Diese Eingabe ist nicht erlaubt!" << std::endl;
        return 1;
    }
    return 0;
}