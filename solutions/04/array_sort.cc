#include <iostream>
#include <iomanip>


// size_t is a special name for a certain integer that is used to index arrays
// which integer really is chosen depends on the platform and the compiler
// for now assume it is unsigned int
//
// we have to pass the size of the array because an array "forgets" its size
//  when passed to a function
void sort_array(int a[], size_t size);
void print_array(int a[], size_t size);
void swap_in_array(int a[], size_t lhs, size_t rhs);


int main()
{
    int a[] = {12, 6, 10, 2, 1, 22, 4, 16, 12, 7};

    // the size of this array is 10
    // we could hard code that or calculate it instead
    // the number of elements is: size of the array in bytes / size of one element in bytes
    sort_array(a, sizeof(a) / sizeof(int));
    print_array(a, sizeof(a) / sizeof(int));
}

void sort_array(int a[], size_t size)
{
    for(size_t replace_index = 0; replace_index < size; ++replace_index)
    {
        size_t min_index = replace_index;
        for(size_t min_candidate_index = replace_index + 1; min_candidate_index < size; ++min_candidate_index)
        {
            if(a[min_candidate_index] < a[min_index])
            {
                min_index = min_candidate_index;
            }
        }

        swap_in_array(a, min_index, replace_index);
    }
}

void print_array(int a[], size_t size)
{
    for(size_t i = 0; i < size; ++i)
    {
        std::cout << "Eintrag " << std::setw(2) << i << ": " << std::setw(3) << a[i] << std::endl;
    }
}

void swap_in_array(int a[], size_t lhs, size_t rhs)
{
    int tmp = a[lhs];
    a[lhs] = a[rhs];
    a[rhs] = tmp;
}