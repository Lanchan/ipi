#include <iostream>
#include <cmath>

// a) we choose f(x) = x^2 - c
//      => f(x) = 0
//     <=> x = sqrt(c)

bool not_close_enough(double c, double x)
{
    return std::abs(c - x * x) > 1E-12;
}

void print_step(bool print, int steps, double x)
{
    if(print)
    {
        std::cout.width(4);
        std::cout << steps << ": " << "x=";
        std::cout.precision(13);
        std::cout << x << std::endl;
    }
}

double newton_method(double c, double x=4, bool print=false)
{
    int steps = 0;
    print_step(print, steps, x);

    while(not_close_enough(c, x))
    {
        x = x - (x*x - c) / (2*x);
        ++steps;

        print_step(print, steps, x);
    }

    return x;
}

double bisection_method(double c, double high=4, bool print=false)
{
    int steps = 0;
    double low = 0;
    double x = (high + low) / 2;
    print_step(print, steps, x);

    while(not_close_enough(c, x))
    {
        double y = x * x - c;
        ++steps;

        if(y > 0)
        {
            high = x;
        }
        else if(y < 0)
        {
            low = x;
        }

        x = (high + low) / 2;

        print_step(print, steps, x);
    }

    return x;
}

void test_for(double c)
{
    std::cout << "calculating sqrt(" << c << ")" << std::endl << std::endl;

    std::cout << "newton method" << std::endl;
    newton_method(c, 4, true);

    std::cout << std::endl << "bisection method" << std::endl;
    bisection_method(c, 4, true);
    std::cout << std::endl;

}

int main()
{
    test_for(2);
    test_for(3);
    test_for(4);
}