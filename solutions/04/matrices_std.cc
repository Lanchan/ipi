#include <iostream>
#include <vector>


using Matrix = std::vector<std::vector<double>>;
using Vector = std::vector<double>;

Matrix matrix_create(size_t rows, size_t cols)
{
    return Matrix(rows, Vector(cols, 0));
}

void matrix_zero(Matrix& matrix)
{
    for(Vector& row : matrix)
    {
        for(double& element : row)
        {
            element = 0;
        }
    }
}

void matrix_id(Matrix& matrix)
{
    for(size_t i = 0; i < matrix.size(); ++i)
    {
        for(size_t j = 0; j < matrix[i].size(); ++j)
        {
            matrix[i][j] = i == j;
        }
    }
}

void matrix_diagonal(Matrix& matrix, const Vector& diagonal)
{
    matrix_zero(matrix);

    for(size_t i = 0; i < matrix.size(); ++i)
    {
        for(size_t j = 0; j < matrix[i].size(); ++j)
        {
            matrix[i][j] = diagonal[i];
        }
    }
}

Matrix matrix_add(const Matrix& lhs, const Matrix& rhs)
{
    Matrix result = matrix_create(lhs.size(), lhs[0].size());

    for(size_t i = 0; i < lhs.size(); ++i)
    {
        for(size_t j = 0; j < lhs[i].size(); ++j)
        {
            result[i][j] = lhs[i][j] + rhs[i][j];
        }
    }
    return result;
}

Matrix matrix_subtract(const Matrix& lhs, const Matrix& rhs)
{
    Matrix result = matrix_create(lhs.size(), lhs[0].size());

    for(size_t i = 0; i < lhs.size(); ++i)
    {
        for(size_t j = 0; j < lhs[i].size(); ++j)
        {
            result[i][j] = lhs[i][j] - rhs[i][j];
        }
    }
    return result;
}

Matrix matrix_transpose(const Matrix& matrix)
{
    Matrix result = matrix_create(matrix.size(), matrix[0].size());

    for(size_t i = 0; i < matrix.size(); ++i)
    {
        for(size_t j = 0; j < matrix[i].size(); ++j)
        {
            result[i][j] = matrix[j][i];
        }
    }
    return result;
}

double matrix_trace(const Matrix& matrix)
{
    double trace = 0;
    for(size_t i = 0; i < matrix.size(); ++i)
    {
        trace += matrix[i][i];
    }
    return trace;
}

Matrix matrix_mult(const Matrix& lhs, const Matrix& rhs)
{
    Matrix result = matrix_create(lhs.size(), rhs[0].size());

    for(size_t i = 0; i < lhs.size(); ++i)
    {
        for(size_t j = 0; j < rhs[0].size(); ++j)
        {
            for(size_t k = 0; k < lhs[i].size(); ++k)
            {
                result[i][j] += lhs[i][k] * rhs[k][j];
            }
        }
    }
    return result;
}

void matrix_print(const Matrix& matrix)
{
    for(const Vector& row : matrix)
    {
        std::cout << "|";
        for(double element : row)
        {
            std::cout.width(4);
            std::cout << element << " ";
        }
        std::cout << "|" << std::endl;
    }
    std::cout << std::endl;
}

int main()
{
    Matrix a = {
        {2, 5, 4},
        {3, 2, 4},
        {1, 1, 0}
    };

    Matrix b = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
    std::cout << "A" << std::endl;
    matrix_print(a);
    std::cout << "B" << std::endl;
    matrix_print(b);

    Matrix result = matrix_create(3, 3);
    matrix_zero(result);

    std::cout << "zero" << std::endl;
    matrix_print(result);

    std::cout << "id" << std::endl;
    matrix_id(result);
    matrix_print(result);

    std::cout << "diagonal" << std::endl;
    matrix_diagonal(result, {1, 2, 3});
    matrix_print(result);

    std::cout << "A + B" << std::endl;
    result = matrix_add(a, b);
    matrix_print(result);

    std::cout << "A - B" << std::endl;
    result = matrix_subtract(a, b);
    matrix_print(result);

    std::cout << "B transposed" << std::endl;
    result = matrix_transpose(b);
    matrix_print(result);

    std::cout << "trace of B transposed: " << matrix_trace(result) << std::endl << std::endl;

    std::cout << "A x B" << std::endl;
    result = matrix_mult(a, b);
    matrix_print(result);

    Matrix x = {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11}
    };

    Matrix y = {
        {0, 4, 8, 12, 16},
        {1, 5, 9, 13, 17},
        {2, 6, 10, 14, 18},
        {3, 7, 11, 15, 19}
    };

    std::cout << "X" << std::endl;
    matrix_print(x);

    std::cout << "Y" << std::endl;
    matrix_print(y);

    std::cout << "X * Y" << std::endl;
    result = matrix_mult(x, y);
    matrix_print(result);
}