#include <iostream>


const int N = 10;
using Matrix = double[N][N];
using Vector = double[N];


void matrix_zero(Matrix matrix, size_t size)
{
    for(size_t i = 0; i < size; ++i)
    {
        for(size_t j = 0; j < size; ++j)
        {
            matrix[i][j] = 0;
        }
    }
}

void matrix_id(Matrix matrix, size_t size)
{
    for(size_t i = 0; i < size; ++i)
    {
        for(size_t j = 0; j < size; ++j)
        {
            matrix[i][j] = i == j;
        }
    }
}

void matrix_diagonal(Matrix matrix, size_t size, const Vector diagonal)
{
    for(size_t i = 0; i < size; ++i)
    {
        matrix[i][i] = diagonal[i];
    }
}

void matrix_add(Matrix lhs, Matrix rhs, size_t size, Matrix result)
{
    matrix_zero(result, size);

    for(size_t i = 0; i < size; ++i)
    {
        for(size_t j = 0; j < size; ++j)
        {
            result[i][j] = lhs[i][j] + rhs[i][j];
        }
    }
}

void matrix_subtract(Matrix lhs, Matrix rhs, size_t size, Matrix result)
{
    matrix_zero(result, size);

    for(size_t i = 0; i < size; ++i)
    {
        for(size_t j = 0; j < size; ++j)
        {
            result[i][j] = lhs[i][j] - rhs[i][j];
        }
    }
}

void matrix_transpose(Matrix matrix, size_t size, Matrix result)
{
    matrix_zero(result, size);

    for(size_t i = 0; i < size; ++i)
    {
        for(size_t j = 0; j < size; ++j)
        {
            result[i][j] = matrix[j][i];
        }
    }
}

double matrix_trace(Matrix matrix, size_t size)
{
    double trace = 0;
    for(size_t i = 0; i < size; ++i)
    {
        trace += matrix[i][i];
    }
    return trace;
}

void matrix_mult(Matrix lhs, size_t lrows, size_t lcols, Matrix rhs, size_t rrows, size_t rcols, Matrix result)
{
    if(lcols != rrows)
    {
        std::cerr << "cannot multiply" << std::endl;
        return;
    }

    matrix_zero(result, lcols);

    for(size_t i = 0; i < lrows; ++i)
    {
        for(size_t j = 0; j < rcols; ++j)
        {
            for(size_t k = 0; k < lcols; ++k)
            {
                result[i][j] += lhs[i][k] * rhs[k][j];
            }
        }
    }
}

void matrix_print(Matrix matrix, size_t rows, size_t cols=0)
{
    if(cols == 0)
    {
        cols = rows;
    }
    for(size_t i = 0; i < rows; ++i)
    {
        std::cout << "|";
        for(size_t j = 0; j < cols; ++j)
        {
            std::cout.width(4);
            std::cout << matrix[i][j] << " ";
        }
        std::cout << "|" << std::endl;
    }
    std::cout << std::endl;
}

int main()
{
    Matrix a = {
        {2, 5, 4},
        {3, 2, 4},
        {1, 1, 0}
    };

    Matrix b = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    Matrix result;

    std::cout << "A" << std::endl;
    matrix_print(a, 3);
    std::cout << "B" << std::endl;
    matrix_print(b, 3);

    std::cout << "zero" << std::endl;
    matrix_zero(result, 3);
    matrix_print(result, 3);

    std::cout << "id" << std::endl;
    matrix_id(result, 3);
    matrix_print(result, 3);

    std::cout << "diagonal" << std::endl;
    Vector diagonal = {1, 2, 3};
    matrix_diagonal(result, 3, diagonal);
    matrix_print(result, 3);

    std::cout << "A + B" << std::endl;
    matrix_add(a, b, 3, result);
    matrix_print(result, 3);

    std::cout << "A - B" << std::endl;
    matrix_subtract(a, b, 3, result);
    matrix_print(result, 3);

    std::cout << "B transposed" << std::endl;
    matrix_transpose(b, 3, result);
    matrix_print(result, 3);

    std::cout << "trace of B transposed: " << matrix_trace(result, 3) << std::endl << std::endl;

    std::cout << "A * B" << std::endl;
    matrix_mult(a, 3, 3, b, 3, 3, result);
    matrix_print(result, 3);

    Matrix x = {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11}
    };

    Matrix y = {
        {0, 4, 8, 12, 16},
        {1, 5, 9, 13, 17},
        {2, 6, 10, 14, 18},
        {3, 7, 11, 15, 19}
    };

    std::cout << "X" << std::endl;
    matrix_print(x, 3, 4);

    std::cout << "Y" << std::endl;
    matrix_print(y, 4, 5);

    std::cout << "X * Y" << std::endl;
    matrix_mult(x, 3, 4, y, 4, 5, result);
    matrix_print(result, 3, 5);
}