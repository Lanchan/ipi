#include <iostream>
#include <vector>
#include <cassert>


struct Matrix
{
    double* data = nullptr;
    size_t rows = 0;
    size_t cols = 0;
};

double& set(Matrix& matrix, size_t row, size_t col)
{
    return matrix.data[matrix.cols * row + col];
}

double get(const Matrix& matrix, size_t row, size_t col)
{
    return matrix.data[matrix.cols * row + col];
}

Matrix matrix_create(size_t rows, size_t cols)
{
    return {
        new double[rows * cols],
        rows,
        cols
    };
}

void matrix_delete(Matrix& matrix)
{
    delete[] matrix.data;
    matrix.data = nullptr;
    matrix.rows = 0;
    matrix.cols = 0;
}

void matrix_zero(Matrix& matrix)
{
    size_t size = matrix.rows * matrix.cols;
    for(size_t i = 0; i < size; ++i)
    {
        matrix.data[i] = 0;
    }
}

void matrix_id(Matrix& matrix)
{
    matrix_zero(matrix);

    size_t size = matrix.rows * matrix.cols;
    for(size_t i = 0; i < size; i += matrix.cols + 1)
    {
        matrix.data[i] = 1;
    }
}

void matrix_diagonal(Matrix& matrix, const std::vector<double>& diagonal)
{
    assert(diagonal.size() <= matrix.rows && diagonal.size() <= matrix.cols);

    matrix_zero(matrix);

    size_t index = 0;
    for(double element : diagonal)
    {
        matrix.data[index] = element;
        index += matrix.cols + 1;
    }
}

Matrix matrix_add(const Matrix& lhs, const Matrix& rhs)
{
    assert(lhs.rows == rhs.rows && lhs.cols == rhs.cols);

    Matrix result = matrix_create(lhs.rows, lhs.cols);
    size_t size = lhs.rows * lhs.cols;

    for(size_t i = 0; i < size; ++i)
    {
        result.data[i] = lhs.data[i] + rhs.data[i];
    }
    return result;
}

Matrix matrix_subtract(const Matrix& lhs, const Matrix& rhs)
{
    assert(lhs.rows == rhs.rows && lhs.cols == rhs.cols);

    Matrix result = matrix_create(lhs.rows, lhs.cols);
    size_t size = lhs.rows * lhs.cols;

    for(size_t i = 0; i < size; ++i)
    {
        result.data[i] = lhs.data[i] - rhs.data[i];
    }
    return result;
}

Matrix matrix_transpose(const Matrix& matrix)
{
    Matrix result = matrix_create(matrix.cols, matrix.rows);
    for(size_t row = 0; row < matrix.rows; ++row)
    {
        for(size_t col = 0; col < matrix.cols; ++col)
        {
            set(result, col, row) = get(matrix, row, col);
        }
    }
    return result;
}

double matrix_trace(const Matrix& matrix)
{
    size_t size = matrix.rows * matrix.cols;

    double trace = 0;

    for(size_t i = 0; i < size; i += matrix.cols + 1)
    {
        trace += matrix.data[i];
    }

    return trace;
}

Matrix matrix_mult(const Matrix& lhs, const Matrix& rhs)
{
    assert(lhs.cols == rhs.rows);

    Matrix result = matrix_create(lhs.rows, rhs.cols);
    matrix_zero(result);

    for(size_t i = 0; i < lhs.rows; ++i)
    {
        for(size_t j = 0; j < rhs.cols; ++j)
        {
            for(size_t k = 0; k < lhs.cols; ++k)
            {
                set(result, i, j) += get(lhs, i, k) * get(rhs, k, j);
            }
        }
    }
    return result;
}

void matrix_print(const Matrix& matrix)
{
    for(size_t row = 0; row < matrix.rows; ++row)
    {
        std::cout << "|";
        for(size_t col = 0; col < matrix.cols; ++col)
        {
            std::cout.width(4);
            std::cout << get(matrix, row, col) << " ";
        }
        std::cout << "|" << std::endl;
    }
    std::cout << std::endl;
}

int main()
{
    Matrix a = matrix_create(3, 3);
    Matrix b = matrix_create(3, 3);
    a.data[0] = 2; a.data[1] = 5; a.data[2] = 4;
    a.data[3] = 3; a.data[4] = 2; a.data[5] = 4;
    a.data[6] = 1; a.data[7] = 1; a.data[8] = 0;

    b.data[0] = 1; b.data[1] = 2; b.data[2] = 3;
    b.data[3] = 4; b.data[4] = 5; b.data[5] = 6;
    b.data[6] = 7; b.data[7] = 8; b.data[8] = 9;

    std::cout << "A" << std::endl;
    matrix_print(a);
    std::cout << "B" << std::endl;
    matrix_print(b);

    Matrix result = matrix_create(3, 3);
    matrix_zero(result);

    std::cout << "zero" << std::endl;
    matrix_print(result);

    std::cout << "id" << std::endl;
    matrix_id(result);
    matrix_print(result);

    std::cout << "diagonal" << std::endl;
    matrix_diagonal(result, {1, 2, 3});
    matrix_print(result);

    std::cout << "A + B" << std::endl;
    matrix_delete(result);
    result = matrix_add(a, b);
    matrix_print(result);

    std::cout << "A - B" << std::endl;
    matrix_delete(result);
    result = matrix_subtract(a, b);
    matrix_print(result);

    std::cout << "B transposed" << std::endl;
    matrix_delete(result);
    result = matrix_transpose(b);
    matrix_print(result);

    std::cout << "trace of B transposed: " << matrix_trace(result) << std::endl << std::endl;

    std::cout << "A x B" << std::endl;
    matrix_delete(result);
    result = matrix_mult(a, b);
    matrix_print(result);

    Matrix x = matrix_create(3, 4);
    Matrix y = matrix_create(4, 5);

    x.data[0] = 0; x.data[1] = 1; x.data[2] = 2; x.data[3] = 3;
    x.data[4] = 4; x.data[5] = 5; x.data[6] = 6; x.data[7] = 7;
    x.data[8] = 8; x.data[9] = 9; x.data[10] = 10; x.data[11] = 11;

    y.data[0] = 0; y.data[1] = 4; y.data[2] = 8; y.data[3] = 12; y.data[4] = 16;
    y.data[5] = 1; y.data[6] = 5; y.data[7] = 9; y.data[8] = 13; y.data[9] = 17;
    y.data[10] = 2; y.data[11] = 6; y.data[12] = 10; y.data[13] = 14; y.data[14] = 18;
    y.data[15] = 3; y.data[16] = 7; y.data[17] = 11; y.data[18] = 15; y.data[19] = 19;

    std::cout << "X" << std::endl;
    matrix_print(x);

    std::cout << "Y" << std::endl;
    matrix_print(y);

    std::cout << "X * Y" << std::endl;
    matrix_delete(result);
    result = matrix_mult(x, y);
    matrix_print(result);
}