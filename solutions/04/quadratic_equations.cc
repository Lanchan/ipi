#include <iostream>
#include <cmath>


double discriminant(double a, double b, double c)
{
    return b*b - 4*a*c;
}

int number_of_solutions(double a, double b, double c)
{
    double d = discriminant(a, b, c);
    if(d > 1E-9)
    {
        return 2;
    }
    if(d < -1E-9)
    {
        return 0;
    }
    return 1;
}

double calc_one_solution(double a, double b)
{
    return -b / 2 / a;
}

double calc_two_solution_plus(double a, double b, double c)
{
    return (-b + std::sqrt(discriminant(a, b, c))) / (2 * a);
}

double calc_two_solution_minus(double a, double b, double c)
{
    return (-b - std::sqrt(discriminant(a, b, c))) / (2 * a);
}

int main()
{
    double a;
    double b;
    double c;

    std::cout << "enter parameters ax^2  + bx + c" << std::endl;

    std::cout << "a: ";
    std::cin >> a;

    std::cout << "b: ";
    std::cin >> b;

    std::cout << "c: ";
    std::cin >> c;

    switch(number_of_solutions(a, b, c))
    {
    case 0:
        std::cout << "no real solutions" << std::endl;
        break;
    case 1:
        std::cout << "one solution: x = " << calc_one_solution(a, b) << std::endl;
        break;
    case 2:
        std::cout << "two solutions: x in {";
        std::cout << calc_two_solution_plus(a, b, c) << ", ";
        std::cout << calc_two_solution_minus(a, b, c) << "}" << std::endl;
        break;
    }
}
