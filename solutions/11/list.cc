#include "list.h"

List::List()
{}

List::List(const List& copy)
{
    copy_from(copy);
}

List& List::operator=(const List& copy)
{
    copy_from(copy);
    return *this;
}

List::~List()
{
    remove();
}

void List::copy_from(const List& copy)
{
    remove();
    Item* current = copy.head;
    while(current != nullptr)
    {
        push_back(current->value);
        current = current->next;
    }
}

void List::push_back(double value)
{
    Item* item = new Item{value, nullptr, nullptr};

    if(head == nullptr)
    {
        head = tail = item;
    }
    else
    {
        tail->next = item;
        item->prev = tail;
        tail = item;
    }
}

bool List::isEmpty() const
{
    return tail == nullptr;
}

void List::remove()
{
    Item* current = head;
    while(current != nullptr)
    {
        Item* next = current->next;
        delete current;
        current = next;

    }
    head = tail = nullptr;
}

List::Iterator List::first()
{
    return {head};
}

List::Iterator List::last()
{
    return {nullptr};
}

List::Iterator::Iterator(List::Item* item)
    : current(item)
{}

List::Iterator& List::Iterator::operator++()
{
    current = current->next;
    return *this;
}

double List::Iterator::printValue() const
{
    return current->value;
}

bool List::Iterator::operator!=(const Iterator& other) const
{
    return current != other.current;
}
