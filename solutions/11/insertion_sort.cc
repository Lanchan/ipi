#include <iostream>
#include <iomanip>
#include <vector>
#include <list>


template<typename T>
void insertion_sort(std::vector<T>& vector)
{
    size_t i = 1;
    while(i < vector.size())
    {
        T x = vector[i];
        int j = int(i - 1);

        while(j >= 0 && vector[j] > x)
        {
            vector[j + 1] = vector[j];
            --j;
        }

        vector[j + 1] = x;
        ++i;
    }
}

template<typename T>
void insertion_sort(std::list<T>& list)
{
    if(list.begin() == list.end())
    {
        return;
    }

    auto it = list.begin();
    ++it;

    while(it != list.end())
    {
        T x = *it;
        auto inner = it;
        --inner;

        while(inner != list.begin() && *inner > x)
        {
            auto up = inner;
            ++up;
            *up = *inner;
            --inner;
        }

        if(*inner > x)
        {
            auto up = inner;
            ++up;
            *up = *inner;
            --inner;
        }

        auto up = inner;
        ++up;
        *up = x;
        ++it;
    }
}

template<typename Iter>
void insertion_sort(Iter begin, Iter end)
{
    if(begin == end)
    {
        return;
    }
    Iter it = begin;
    ++it;

    while(it != end)
    {
        auto x = *it;
        auto inner = it;
        --inner;

        while(inner != begin && *inner > x)
        {
            auto up = inner;
            ++up;
            *up = *inner;
            --inner;
        }

        if(*inner > x)
        {
            auto up = inner;
            ++up;
            *up = *inner;
            --inner;
        }

        auto up = inner;
        ++up;
        *up = x;
        ++it;
    }
}

template<typename Iter>
bool is_sorted(Iter begin, Iter end)
{
    Iter right = begin;
    while(right != end)
    {
        auto left = *begin;
        if(left > *right)
        {
            return false;
        }
        ++right;
    }
    return true;
}

template<typename Iter>
void print(Iter begin, Iter end)
{
    while(begin != end)
    {
        std::cout << *begin << ", ";
        ++begin;
    }
    std::cout << std::endl;
}

template<typename T>
void test(std::vector<T> v)
{
    insertion_sort(v);
    std::cout << "vector sorted: " << std::boolalpha << is_sorted(v.begin(), v.end()) << "  ->  ";
    print(v.begin(), v.end());

    std::list<T> list(v.begin(), v.end());
    insertion_sort(list);

    std::cout << "list sorted  : " << std::boolalpha << is_sorted(v.begin(), v.end()) << "  ->  ";
    print(list.begin(), list.end());

    std::cout << std::endl;
}

int main()
{
    test<double>({2, 5, 4, -7, 12, 1});
    test<std::string>({"h", "a", "l", "l", "o"});
    test<const char*>({"h", "a", "l", "l", "o"});
    test<int>({2, 5, 4, -7, 12, 1});
    test<char>({'h', 'a', 'l', 'l', 'o'});

    int array[] = {-1, 1, -2, 2, -3, 3};
    std::vector<int> vector = {-1, 1, -2, 2, -3, 3};
    std::list<int> list = {-1, 1, 2, -2, 3, -3};
    std::string string = "Reliefpfeiler";

    insertion_sort(array, array + 6);
    insertion_sort(vector.begin(), vector.end());
    insertion_sort(list);
    insertion_sort(string.begin(), string.end());

    std::cout << "iter sort array : " << is_sorted(array, array + 6) << "  ->  ";
    print(array, array + 6);
    std::cout << "iter sort vector: " << is_sorted(vector.begin(), vector.end()) << "  ->  ";
    print(vector.begin(), vector.end());
    std::cout << "iter sort list  : " << is_sorted(list.begin(), list.end()) << "  ->  ";
    print(list.begin(), list.end());
    std::cout << "iter sort string: " << is_sorted(string.begin(), string.end()) << "  ->  ";
    print(string.begin(), string.end());
}