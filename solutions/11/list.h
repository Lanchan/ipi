#ifndef LIST_H
#define LIST_H

class List
{
private:

    struct Item
    {
        double value;
        Item* next;
        Item* prev;

        friend class Iterator;
    };

    Item* head = nullptr;
    Item* tail = nullptr;

    void copy_from(const List&);

public:
    List();
    List(const List&);
    List& operator=(const List&);
    ~List();

    void push_back(double);
    bool isEmpty() const;
    void remove();

    class Iterator
    {
    private:
        Item* current;
    public:
        Iterator(Item*);
        Iterator& operator++();
        double printValue() const;
        bool operator!=(const Iterator&) const;
    };

    Iterator first();
    Iterator last();
};

#endif
