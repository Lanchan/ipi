#include <iostream>
#include <list>
#include <ctime>
#include <vector>
#include <iomanip>

#include "list.h"

double start_time;

void tic()
{
    start_time = clock();
}

double toc()
{
    return (clock() - start_time) / CLOCKS_PER_SEC;
}

void speed_test_my_list(int N)
{
    List my_list;

    for(int i = 1; i <= N; ++i)
    {
        my_list.push_back(i);
    }

    if(my_list.isEmpty())
    {
        std::cerr << "Die List ist leer!" << std::endl;
    }

    double sum = 0;
    for(List::Iterator it = my_list.first(); it != my_list.last(); ++it)
    {
        sum += it.printValue();
    }

    my_list.remove();
}

void speed_test_stl_list(int N)
{
    std::list<double> my_list;

    for(int i = 1; i <= N; ++i)
    {
        my_list.push_back(i);
    }

    if(my_list.empty())
    {
        std::cerr << "Die List ist leer!" << std::endl;
    }

    double sum = 0;
    for(double d : my_list)
    {
        sum += d;
    }

    my_list.clear();
}

int main()
{
    std::vector<double> my_list_time;
    std::vector<double> stl_list_time;

    for(int N = 100000; N <= 10000000; N += 100000)
    {
        tic();
        speed_test_my_list(N);
        my_list_time.push_back(toc());

        tic();
        speed_test_stl_list(N);
        stl_list_time.push_back(toc());
    }


    std::cout << "  my_list     stl_list" << std::endl;
    for(size_t i = 0; i < my_list_time.size(); ++i)
    {
        std::cout << std::setw(10) << my_list_time[i] << "  ";
        std::cout << std::setw(10) << stl_list_time[i] << "  ";
        std::cout << std::setw(10) << stl_list_time[i] / my_list_time[i] << std::endl;
    }
}
