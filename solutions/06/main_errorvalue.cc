#include <iostream>

#include "errorvalue.h"


int main()
{
    ErrorValue a(10.0, 2.0);
    ErrorValue b(5.0, 1.0);

    ErrorValue s = a + b;
    std::cout << s.value() << " +- " << s.absolute();
    std::cout << " (" << (s.relative() * 100 ) << "%)" << std::endl;

    double measurement_a[] = {10.0, 10.2, 10.8, 10.1, 9.6, 10.2, 9.9, 10.3};
    double measurement_b[] = {14.2, 15.0, 13.8, 14.5, 14.8, 14.5, 13.9, 14.1, 14.5, 14.2};
    double measurement_c[] = {11.3, 10.8, 11.1, 11.0, 11.6, 10.8, 10.7, 11.8, 12.0};

    ErrorValue error_a = ErrorValue::fromMeasurements(measurement_a, 8);
    ErrorValue error_b = ErrorValue::fromMeasurements(measurement_b, 10);
    ErrorValue error_c = ErrorValue::fromMeasurements(measurement_c, 9);

    ErrorValue error_d = error_a + error_b + error_c;
    std::cout << error_d.value() << " +- " << error_d.absolute();
    std::cout << " (" << (error_d.relative() * 100 ) << "%)" << std::endl;
}