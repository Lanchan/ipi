#pragma once

class MyVector
{
private:
    float x,y,z;
  
public:
    MyVector();
    MyVector(float x, float y, float z);
    // we can delete this or use the default destructor
    ~MyVector() = default;

    // this should be const
    void print() const;

    // these cannot be cont
    void setValues(float x, float y, float z);
    void setX(float x);
    void setY(float y);
    void setZ(float z);

    // the rest is const again
    float getX() const;
    float getY() const;
    float getZ() const;

    float length() const;

    MyVector add(const MyVector& vec) const;
    MyVector subtract(const MyVector& vec) const;

    MyVector mult(float scale) const;
    float dot(const MyVector& vec) const;
    MyVector cross(const MyVector& vec) const;

    float angle(const MyVector& vec) const;
    float area(const MyVector& vec) const;

    bool isOrthogonal(const MyVector& vec) const;
    bool isParallel(const MyVector& vec) const;
};
