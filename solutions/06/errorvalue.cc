#include <cmath>

#include "errorvalue.h"

ErrorValue::ErrorValue(double mean, double deviation)
    : mean(mean), deviation(deviation)
{}

ErrorValue ErrorValue::operator+(const ErrorValue &other) const
{
    return {
        mean + other.mean,
        std::sqrt(deviation*deviation + other.deviation*other.deviation)
    };
}

double ErrorValue::value() const
{
    return mean;
}

double ErrorValue::absolute() const
{
    return deviation;
}

double ErrorValue::relative() const
{
    return absolute() / mean;
}

ErrorValue ErrorValue::fromMeasurements(double* measurements, int size)
{
    double mean = 0;
    for(int i = 0; i < size; ++i)
    {
        mean += measurements[i];
    }
    mean /= size;

    double deviation = 0;
    for(int i = 0; i < size; ++i)
    {
        deviation += std::pow(measurements[i] - mean, 2);
    }
    deviation = std::sqrt(deviation / (size - 1));

    return {mean, deviation};
}
