#pragma once

class ErrorValue
{
    double mean;
    double deviation;

public:
    // we need this because of line 3: ErrorValue a(10.0, 2.0)
    // -> a constructor taking 2 double arguments
    ErrorValue(double mean, double deviation);

    // line 7: a + b
    // -> operator+ for ErrorValue and ErrorValue
    ErrorValue operator+(const ErrorValue& other) const;
    // line 8: s.value()
    double value() const;
    // line 8: s.absolute()
    double absolute() const;
    // line 9: s.relative()
    double relative() const;

    static ErrorValue fromMeasurements(double* measurements, int size);
};

