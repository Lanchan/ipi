#include <iostream>
#include <cmath>

#include "myvector.h"


MyVector::MyVector()
    : x(0), y(0), z(0)
{}

MyVector::MyVector(float x, float y, float z)
    : x(x), y(y), z(z)
{}

void MyVector::print() const
{
    std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
}

void MyVector::setValues(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

void MyVector::setX(float x)
{
    this->x = x;
}

void MyVector::setY(float y)
{
    this->y = y;
}

void MyVector::setZ(float z)
{
    this->z = z;
}

float MyVector::getX() const
{
    return x;
}

float MyVector::getY() const
{
    return y;
}

float MyVector::getZ() const
{
    return z;
}

float MyVector::length() const
{
    return std::sqrt(x*x + y*y + z*z);
}

MyVector MyVector::add(const MyVector &vec) const
{
    return {
        x + vec.x,
        y + vec.y,
        z + vec.z
    };
}

MyVector MyVector::subtract(const MyVector &vec) const
{
    return {
        x - vec.x,
        y - vec.y,
        z - vec.z
    };
}

MyVector MyVector::mult(float scale) const
{
    return {
        x * scale,
        y * scale,
        z * scale,
    };
}

float MyVector::dot(const MyVector &vec) const
{
    return x*vec.x + y*vec.y + z*vec.z;
}

MyVector MyVector::cross(const MyVector &vec) const
{
    return {
        y*vec.z - z*vec.y,
        z*vec.x - x*vec.z,
        x*vec.y - y*vec.x
    };
}

float MyVector::angle(const MyVector &vec) const
{
    return std::acos(dot(vec) / length() / vec.length());
}

float MyVector::area(const MyVector &vec) const
{
    return cross(vec).length();
}

bool MyVector::isOrthogonal(const MyVector &vec) const
{
    return std::abs(dot(vec)) < 1E-9;
}

bool MyVector::isParallel(const MyVector &vec) const
{
    float factor = x / vec.x;
    return y / factor == vec.y && z / factor == vec.z;
}
