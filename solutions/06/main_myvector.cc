#include <iostream>

#include "myvector.h"

const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

float rad2deg(float rad)
{
    return rad * 180 / PI;
}

int main()
{
    MyVector a(5, 0, 2);
    MyVector b;
    b.setX(3);
    b.setY(1);
    b.setZ(4);
    MyVector c;
    c.setValues(5, 3, 5);

    MyVector ab = b.subtract(a);
    MyVector ac = c.subtract(a);
    MyVector bc = c.subtract(b);

    bool right_angled = ab.isOrthogonal(ac) || ac.isOrthogonal(bc) || bc.isOrthogonal(ab);
    std::cout << "triangle is right angled: " << std::boolalpha << right_angled << std::endl;

    float angle_a = ab.angle(ac);
    float angle_b = ab.angle(bc);
    float angle_c = ac.angle(bc);
    bool isosceles = angle_a == angle_b || angle_b == angle_c || angle_c == angle_a;
    std::cout << "triangle is isosceles: " << std::boolalpha << isosceles << std::endl;

    std::cout << "angle at a is: " << rad2deg(angle_a) << " degree" << std::endl;
    std::cout << "angle at b is: " << rad2deg(angle_b) << " degree" << std::endl;
    std::cout << "angle at c is: " << rad2deg(angle_c) << " degree" << std::endl;

    float area = ab.area(ac) / 2;
    std::cout << "area: " << area << std::endl;

    MyVector ab_doubled = b.mult(2).subtract(a.mult(2));
    MyVector ac_doubled = c.mult(2).subtract(a.mult(2));

    float doubled_area = ab_doubled.area(ac_doubled) / 2;
    std::cout << "area for doubled vectors is " << doubled_area / area << " times as large" << std::endl;
}