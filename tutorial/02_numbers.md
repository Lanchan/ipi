02: Numbers
===========

Integer numbers
---------------

It is very boring to only output static messages. Now we will use some actual numbers and do some simple calculations.

```c++
#include <iostream>

int main()
{
    std::cout << 1 + 2 << std::endl;    // output: 3
    std::cout << 2 * 3 << std::endl;    // output: 6
    std::cout << 11 % 2 << std::endl;   // output: 1
}
```

In this example we have `C++` do some simple calculations for us.

#### Exercise 02.1

Add these two lines:

```c++
std::cout << "1 + 2" << std::endl;
std::cout << 1 + 2 << std::endl;
```

What is the output of these two lines and why?

#### Exercise 02.2

Divide some numbers:

```c++
std::cout << 10 / 2 << std::endl;
std::cout << 10 / 5 << std::endl;
std::cout << 10 / 3 << std::endl;
```

How does the result from the last line differ from the other lines?


Floating-point numbers
----------------------

Sometimes we need decimal values for our calculations. Since `int` can only hold integer numbers we 
need a different data type: `float`

(Note: we are actually using *double* precision floats called `double` in the next example)

```c++
#include <iostream>

int main()
{
    std::cout << 1.0 + 2.0 << std::endl;    // output: 3
    std::cout << 2.0 * 3.0 << std::endl;    // output: 6
    std::cout << 10.0 / 3.0 << std::endl;   // output: 3.333333 ...
}
```

This time the result for `10.0 / 3.0` is correct. Dividing integers will **always** result in
 *integer division* which will always round towards `0`. If you need a decimal result at least one of the
 operands (in this case numbers) must be of type `float`.
 
 This rule can be generalized: The result of an operation (`+`, `-`, `*`, `/`) will be of type `int`
 if and only if **both** operands are of type `int`.