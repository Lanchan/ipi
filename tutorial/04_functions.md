04: Functions
=============

Often it is necessary to encapsulate behaviour to make it more usable and separate it from the rest of the program.
That's what *functions* are for. We already know the main *function* which must always exist. Apart from the 
main *function* we can create more functions as we like.

```c++
#include <iostream>

int square(int x)
{
    return x * x;
}

int add(int a, int b)
{
    return a + b;
}

int main()
{
    std::cout << "the square of 2 is " << square(2) << std::endl;
    std::cout << "1 plus 2 is " << add(1, 2) << std::endl;
}
```

In this example we *declared* and *defined* a function called `square`.
The result of the function is an integer number.
Inside the parenthesis we see that the function takes one parameter of type `int`
and that the parameter is called `x`.

Inside the body of the function we see that it *returns* the square of `x`.
That means: the resulting value of the function is `x * x`.

Every function that has a type **must** return a value with the main function being the only exception.
(there are functions without a type. Actually they have the type "empty" which is called `void`)

You can also have multiple parameters, see the function `add`. Multiple paremeters must be separated by `,`

Exercises
---------

#### 04.1

Add another `return` statement to the `square` function:

```c++
int square(int x)
{
    return x * x * x;
    return x * x;
}
```

Which result do you get? Is it `x * x` or `x * x * x`? Why do you get this result and not the other one?

#### 04.2

Remove the `return` statement from the square function, compile and run the program.

The result will be a random number!

Now compile with warnings enabled by adding `-Wall` to the compiler.

What do you see? Does this help preventing those "bugs"?

#### 04.3

Write a function with `10` parameters and do some calculations with them (use every parameter).

Now remove one of the parameters from the calculation but leave it in the parameter list. Compile the program with warnings `-Wall` enabled. What do you observe?
Is that a helpful warning?

#### Important Note:

You could also write the `square` function like this:

```c++
int square(int x)
{
    std::cout << x * x << std::endl;
}
```

The problem here is: If you print the result it is gone. You cannot use it for further calculations. **Do not do this!**

For the purpose of this lecture please do not use `std::cout` in any function except from the main function.
(If an exercise specifically tells you to use `std::cout` inside another function you are allowed to do so, of course).

While writing your code you also may use `std::cout` anywhere to print intermediary results but please remove them
in the final version you send to me! Generally only the main function should interact with the user.

In the future we might remove points if you turn in programs that print warnings during compilation or if your
program itself prints messages it should not!