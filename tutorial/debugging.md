How to use a debugger (gdb)
===========================


1. Have a faulty program

    ```cpp
        // main.cpp
        #include <iostream>

        int function(int i)
        {
            return i / 0;
        }

        int main()
        {
            std::cout << function(4) << std::endl;
        }
    ```

2. Compile the program and include debugging symbols:

    ```sh
        g++ -g main.cpp -o main  # on windows always add .exe
    ```

3. Start the debugger and load the program

    ```sh
        gdb main  # on windows again add .exe
    ```

    You should see a line like "Reading symbols from main ... done". You are now inside gdb which is indicated by `(gdb)`

4. Run the program inside the debugger

    Don't actually type the `(gdb)`. Your terminal should have already written it indicating you are inside gdb.

    ```sh
        (gdb) run
    ```

    Now you will see the line that caused the crash. In this example the output might look like this:

    ```sh
        Starting program: /home/user/main 

        Program received signal SIGFPE, Arithmetic exception.
        0x00005555555548f0 in function (i=4) at main.cpp:5
        5       return i / 0;
    ```

    It is easy to see that the error occured in line `5` of the file `main.cpp`. Additionally the code of that line is printed and the parameter the function was called with. If you didn't compile with `-g` you will only see the name of the function that failed - if at all.

5. (Optional) Inspect the call frame

    Sometimes it is useful to know in which context the function failed. Perhaps it failes only in certain cases. With `bt` gdb will list the call stack up to the failure:

    ```sh
        (gdb) bt
    ```

    As we are calling `function` in the main function these two are listed:

    ```
        #0  0x00005555555548f0 in function (i=4) at main.cpp:5
        #1  0x0000555555554902 in main () at main.cpp:10
    ```

    The function at the top is again the failing function but now we see that it was called from the main function. Depending on the program the call stack might be much longer.

6. Close the debugger
    
    - type `quit`
    - or press `CTRL+D`

7. Fix the error
8. ???
9. Profit