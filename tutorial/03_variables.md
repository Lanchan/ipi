03: Variables
=============

Using `std::cout` we can print the result of a calculation. But we cannot reuse the result in a later
calculation. For that we have to "save" the results. The places where we can "save" those values are
called *variables*. Again, because of static typing, variables **must** have a type.

```c++
#include <iostream>

int main()
{
    int myAge = 42;
    float pi = 3.1415926;
    
    std::cout << "I am " << myAge << " years old." << std::endl;
    std::cout << "On a completely unrelated topic: pi = " << pi << std::endl;
}
```

The syntax of a *variable declaration* is `<type> <variable name>;`. Optionally you can *initialize*
the *variable* with a start value like this `<type> <variable name> = <value>;`

In the example above we *declare* `myAge` to be of type `int` and have an initial value of `42`.

Similarly `pi` is a floating-point number with the well known value (approximately).

*Variables* can be used like literal numbers. So you can - for example - print them onto the console.

It's important to understand that the type of a *variable* does **not** depend on the initialization value. It's
the type of the *declaration* that matters.

```c++
int x = 1.41;  // assign a floating-point number to an integer variable

std::cout << x << std::endl;  // prints 1, and NOT 1.41
```

Here we *declare* `x` to be an integer but we initialize it with a floating-point number. Generally the type of
the *variable* and the type of the initialization value must match. In some cases the initialization can be
converted to match the type of the *variable*. This is called *implicit cast*. Most of the numeric types in `C++`
can be converted into each other with some penalty:

- `float` or `double` to `int`: the value will be rounded towards `0`.
- `int` to `float`: some very large (64bit) integer number cannot be accurately represented by `float` -> loss of precision

### Exercises

#### 03.1 To double quote or not to double quote

What is the difference between these two lines:

```c++
std::cout << "myAge" << std::endl;
std::cout << myAge << std::endl;
```

Observe and explain the output of these lines.

#### 03.2 Words plus numbers

Try these lines (try them separately)

```c++
std::cout << "myAge" + 1 << std::endl;
std::cout << "myAge" + 2 << std::endl;
std::cout << "myAge" + 3 << std::endl;
std::cout << "myAge" + 6 << std::endl;
std::cout << "myAge" + 100000 << std::endl;
```

What is the result of a "word" plus an integer? Why is this not always useful? (Hint: look at the last example)

#### 03.3 

Modify the above program to also print how old i was last year.
- create a new integer variable (think of a suitable name)
- initialize the variable with the age from last year by reusing `myAge`.
- add a line to print the result together with a nice message.

#### 03.4 Area of a circle

Additionally calculate the area of a circle of radius 7cm.
- remember or look up the formula for the area of a circle
- create a new floating-point variable (think of a suitable name)
- initialize the variable by reusing `pi` with the area of the given circle.
- add a line to print the result together with a nice message

#### 03.5 fahrenheit to celsius

Now we want to convert from a temperature in celsius to fahrenheit.
- look up the formula to convert celsius to fahrenheit
- create a variable and initialize it with a temperature in celsius e.g. 37.7°C
  - think of a suitable name for the variable, not just "c"
  - what type do we need here?
  - initialize it with the temperature we want to convert
- create a variable for the result temperature in fahrenheit
  - think of a suitable name for the variable, not just "f"
  - what type do we need here?
  - initialize it by calculating the corresponding temperature in fahrenheit

Then print the initial temperature and the result temperature together with a nice message.

Test values:
- 37°C = 98.6°F
- 0°C = 32°F

