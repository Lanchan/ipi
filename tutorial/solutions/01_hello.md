Exercises:
==========

1
-

If you remove the line `std::cout << "Hello World!" << std::endl;` and compile/run the program you will see
that the output `"Hello World"` is gone. We deleted the line that tells the computer to print it so we don't
see it anymore. 

2
-

`std::endl` is a special symbol that represent a new line. If we remove that no additional line break is inserted
after `"Hello World`

3
-

```c++
#include <iostream>

// program starts here
int main()
{
    std::cout << "Hello World!" << std::endl;
    std::cout << "Hello Niels!" << std::endl;
    std::cout << "Hello Professor Mombaur!" << std::endl;
}
```

4
-

The error message is

```
F:\dev\ipi\scratch\main.cpp: In function 'int main()':
F:\dev\ipi\scratch\main.cpp:7:1: error: expected ';' before '}' token
 }
 ^
 ```
 
 It tells us:
 - the error is in the `main` function
 - the error is in line `7`
 - the compiler expected a `;` before the closing `}`
 
5
-

Error message:

```
C:/PROGRA~1/MINGW-~1/X86_64~2.0-W/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/5.4.0/../../../../x86_64-w64-mingw32/lib/../lib/libmingw32.a(lib64_libmingw32_a-crt0_c.o):crt0_c.c:(.text.startup+0x2e): undefined reference to `WinMain'
   collect2.exe: error: ld returned 1 exit status
```

The important part is `undefined reference to WinMain` (the message might differ on Linux) which means that
a `main` function was declared (by the compiler) and the compiler wants to use it. But as we didn't define
a function with that exact name the compiler can't find it.

