01: Hello World
===============


This is the code for a simple `C++` program. As it simply prints "Hello World!" onto the
console it is also called "Hello World" program.

- Type the code into your favourite text editor. Do **not** simply copy-and-paste it!

```c++
#include <iostream>

// program starts here
int main()
{
    std::cout << "Hello World!" << std::endl;
}
```

- Save the file. Make sure to choose `.cpp` or `.cc` for the extension. E.g. `hello.cpp`.
- Open a terminal in the folder where you saved the file.
- Now compile it with `g++ hello.cpp`
- Run it with `./a.out` on Linux/Mac or `a.exe` on Windows

### What's going on?

```c++
#include <iostream>
```

`#include` tells the compiler to *include* the contents of another file. In this case we *include* the
contents of `iostream` which is part of the standard library of `C++`. In particular `iostream` is needed
for `std::cout` and `std::endl`

```c++
// program starts here
```

This is just a *comment*. It does not do anything. In fact you could remove it and the program would not change.
Comments are useful if you want to document some decisions you made or explain difficult code.

```c++
int main()
```

This tells the compiler that we want to create a *function* called "main". Every program **must** have
exactly one main *function*. As everything in `C++` must have a type ("C++ is statically typed") the
main *function* also needs a type. It is also required that the main *function* has the type `int`.

The parenthesis `()` indicate that this is a *function*. They are empty because there are no arguments. 


```c++
{
    ...
}
```

Every *function* that does something **must** have a *body*. Everything inside the curly braces belongs
to the function.

```c++
std::cout << "Hello World!" << std::endl;
```

If you run the program you can see that the message "Hello World!" is printed onto the console. That's
exactly what this line does.

### Exercises:

These exercises tell you to modify the "Hello World" program from above. For each exercise you should
restore the "Hello World" program before you go on to the next one.

#### 01.1

Remove the line `std::cout << "Hello World!" << std::endl;` and see what happens. Why does it happen?

#### 01.2

Now only remove the last part: `<< std::endl;` but make sure to add a `;`

The line should look like this:

```c++
std::cout << "Hello World!";
```

What do you observe? What does the `std::endl` do?

#### 01.3

Add some more output (always add `std::endl` at the end):

- print "Hello World"
- greet your Tutor
- greet the lecturer

#### 01.4

Remove the `;`  after `std::endl` and find out how the error message you see corresponds to your modification.

#### 01.5

Rename the "main" *function* into "Main" *function* and look at the error message. Does it make sense?

Note: It is very important to read and understand (to a certain degree) what the error message means
so you can solve the error. 