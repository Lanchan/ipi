06 Testing
==========

Most of the time programmers will not actually write code instead they will fix errors in their program. To fix
an error you first have to know that there **is** an error. Checking for errors can easily be done with tests.

Side note
---------

There are two equal signs in `C++`:
- a single `=` : This is actually an assignment like in `i = i + 1`. "Assign the value of `i + 1` to `i`"
- a double `==`: This is a test for equality:
  - `1 + 2 == 3`: the result is `true`
  - `1 == 1.0`: also `true`
  - `2 + 3 == 4`: the result is `false`
  - `1.1 == 1`: also `false`

Using `assert()`
----------------

```c++
#include <iostream>
#include <cassert>

// This is the function i want to test
int sumOfAllNumbersUntilN(int n)
{
    return n + 1 * n / 2;
}

int main()
{
    // these are my test cases:
    //    0 -> 0
    //    1 -> 1
    //    2 -> 1 + 2 = 3
    //    3 -> 1 + 2 + 3 = 6
    //    4 -> 1 + 2 + 3 + 4 = 10
    //  100 -> 1 + ... + 100 =  5050
    
    assert(sumOfAllNumbersUntilN(0) == 0);
    assert(sumOfAllNumbersUntilN(1) == 1);
    assert(sumOfAllNumbersUntilN(2) == 2);
    assert(sumOfAllNumbersUntilN(3) == 3);
    assert(sumOfAllNumbersUntilN(100) == 5050);
}
```

We don't know if the function `sumOfAllNumbersUntilN` is correct. That's why we implemented some tests. At first
we have to include another part of the standard library `cassert` which includes the `assert` function. Note that
`assert` is **not** inside the standard namespace for some reason - `std::assert` does not exist.

`assert` does two things:
- if the condition is `true` - e.g. `assert(true)` or `assert(1 == 1)` or `assert(1 + 2 == 3)` - then `assert`
  does nothing. A successful test should be silent!
- if the condition is `false` - e.g. `assert(false)` or `assert(1 == 2)` or `assert(2 + 3 == 4)` - then `assert`
  calls the function `abort` which will terminate the program and print an error message.
  
The idea is: If everything is correct i don't want to see that there are tests. Otherwise i want the program to exit
in order to find and fix the error.

The error message could look like this (Windows):

```
File: test.cpp, Line 23

Expression: sumOfAllNumbersUntilN(3) == 6

This application has requested the Runtime to terminate it in an unusual way.
Please contact the application's support team for more information.
```

Or like this (Linux):

```
a.out: testing.cpp:23: int main(): Assertion `sumOfAllNumbersUntilN(3)' failed.
Aborted (core dumped)
```

You gain the following information:
- the program crashed, obviously
- the error is in the file `testing.cpp`
- the error happened in line `23`
- the test that failed was `sumOfAllNumbersUntilN(3) == 6`

### Exercises

#### 06.1

Fix the error and make sure all test cases run successfully.


#### 06.2 Is `assert()` broken?

Run the following line:

```
assert(0.1 + 0.2 == 0.3);
```

Why does the test fail? Is it because of `assert` itself?

#### 06.3

Write a correct test for `0.1 + 0.2 == 0.3` by checking if the left-hand side and the right-hand side are **approximately** equal:
- take the difference of both sides
- get the absolute value of the difference. Use `std::abs` from `cmath` for that.
- assert that the absolute difference is smaller than a threshold `1E-10`.  Use the *less than* operator `<`

