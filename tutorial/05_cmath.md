05 cmath
========

The standard library does not only consist of `iostream`. In fact there are many more *header* files you can use.

```c++
#include <iostream>
#include <cmath>

int main()
{
    std::cout << std::cos(90) << std::endl;
    std::cout << std::sin(3.14159) << std::endl;
}
```

`cmath` includes many mathematical function like `sin`, `cos`, `sqrt`. Note that the trigonometric functions
expect the argument to be in radians. That's why the first line prints something like `-0.448` instead of `0`.

The second line print something strange: `2.65359e-006` which is a shortcut for `2.65359 * 10^(-6)` "times 10 to the power of -6".
As we put in something close to `pi` we only get something close to `0`: `0,00000265359`

### Exercises

#### 05.1 degrees to radians

Write a function that takes an angle in degrees and return the angle in radians.
- think of a suitable name for the function
- what is the resulting type of the function?
- think of a suitable name for the parameter
- what is the type of the parameter?
- implement the function by remembering or looking up the formula for the conversion
- *return* the result!
- do not use `std::cout` in the final version of your function!
- assert that your function is correct by printing these test values in the main function
    - 0°   = 0
    - 90°  ~ 1.570796326794897
    - 180° ~ 3.141592653589793
    - 10°  ~ 0.174532925199433


#### 05.2 Haversine Formula (preparations)

Given two geographic coordinates the Haversine Formula can be used to calculate the distance in meters between these points.

A geographic coordinate is a tuple of this form: (AA° BB' CC.CC" N, XX° YY' ZZ.ZZ" E). Before using the coordinates
in the formula we have to convert them to a floating-point angle

Write a function that converts one component of a geographic coordinate into a floating-point angle in degrees.
- remember or lookup the formula for converting an angle given by degrees `°`, minutes `'` and seconds `"` into a decimal number (angle in degree)
- ignore the "N" and "E"
- think of a suitable name for the function
- what are the types of the parameters? (they can be different!)
- calculate the result and return it
- do not use `std::cout` in the final version of your function!
- test the function in your main function using these test cases:
  - Mathematikon: 49° 25' 02.5" N ~ 49.417361° and 8° 40' 31.5" E ~ 8.675417°

Hints:
- remember what happens when you type `1/2` vs `1.0/2.0`
- do **not** include the leading `0` in your program, because that will cause the number to be an octal number (base `8`). This is also known as the most useless feature of `C++` ;)
  
  
#### 05.3 Haversine Formula

For this exercise you need the functions from `05.1` and `05.2`

The Haversine Formula is given below (pseudo code)

```
    The inputs are two coordinates, i.e. four components:
    - latitude of start point in radians
    - longitude of start point in radians
    - latitude of end point in radians
    - longitude of end point in radians
    The result should be the distance between the points in meters

    radius of the earth = 6335439 meters
    
    difference in latitudes = (latitude of end point - latitude of start point) in radians
    difference in longitudes = (longitude of end point - longitude of start point) in radians

    a = sin(difference in latitudes / 2) squared 
      + cos(latitude of start point) * cos(latitude of end point)
      * sin(difference in longitudes / 2) squared
    arc length = 2 * atan2(square root of a, square root of (1 - a));
    distance = radius of the earth * arc length
```

Write a program that calculates the distance between two geographical coordinates
- write a function with appropriate name, parameters and return value using the Haversine Formula
  - do not use `std::cout` in the final version of that function
- write a main function that does the following:
  - Convert these two test points to degree
    - Mathematikon:      49° 25' 02.5" N, 8° 40' 31.5" E
    - Heidelberg Castle: 49° 24' 38.2" N, 8° 42' 55.5" E
  - call your haversine function with the two points i.e. four components
  - print the result (it should be around 2.99 km)
  - calculate the distance between two interesting points and print it together with a nice message